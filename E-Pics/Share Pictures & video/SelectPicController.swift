//
//  SelectPicController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/23/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Photos

class SelectPicController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //Id's
    let celda_id = "celda_id"
    let header_id = "header_id"
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Background
        collectionView.backgroundColor = .white
        
        //Se manda a utilizar la celda
        collectionView.register(PhotoSelectorCell.self, forCellWithReuseIdentifier: celda_id)
        
        //Se manda a crear el header
        collectionView.register(PicSelectorHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: header_id)
    
        //Manda a ejecutar las funciones
        PreparaBtnNav()
        DownloadAllPics()
    }
    
    //Prepara los botones de navegacion
    fileprivate func PreparaBtnNav() {
        
        //Color de la barra de la navegacion
        navigationController?.navigationBar.tintColor = .purple
        
        //Define el boton el lado izquierdo
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(CancelaPic))
        
        //Define el boton el lado derecho
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(PostPic))
    }
    
    //Detecta que items fue seleccionado
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Almacena los valores del los items seleccionados
        self.ImagenSelec = imagesArray[indexPath.item]
        
        //Manda a recargar datos en el collectionview
        self.collectionView.reloadData()
        
        //Lista los index encontrados
        let indexPath = IndexPath(item: 0, section: 0)
        
        //Efecto de scrolltop
        collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
        
    }
    
    //Referencia a los assets
    var assets = [PHAsset]()
    
    //Referencia de imagen seleccionada para pasar la imagen al header
    var ImagenSelec: UIImage?

    //Almacenara las fotos descargadas para pasarlas al  grid del carrete
    var imagesArray = [UIImage]()
    
    //RefreshController
    let RefreshController = UIRefreshControl()
    
    //Accede a las fotos del carrete y las muestra en el controlador
    fileprivate func DownloadAllPics() {
        
        //Define the manger
        let imgManager = PHImageManager.default()
        
        //Define mode of request
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        //Define size
        let targetMedida = CGSize(width: 200, height: 200)
        
        //Define options for assets
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
    
        //Check data from carrete
        if let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions) {
            
            //Comprueba que sea mayor que 0
            if fetchResult.count > 0 {
                for i in 0..<fetchResult.count {
                    
                    let assets = fetchResult.object(at: i)
                    
                    imgManager.requestImage(for: fetchResult.object(at: i) , targetSize: targetMedida, contentMode: .aspectFit, options: requestOptions) { (image, err) in
                        
                        if let images = image {
                            
                            self.assets.append(assets)
                            self.imagesArray.append(images)
                            
                            if self.ImagenSelec == nil {
                                self.ImagenSelec = images
                            }
                            
                            self.collectionView.reloadData()
                        }
                    }
                }
            } else {
                
                print("you got not pics")
                self.collectionView.reloadData()
            }
        }
    }
    
    
    //Se define el numero de items
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    //Se define la celda correspondiente a cada objeto
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Se manda a reutilizar
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: celda_id, for: indexPath) as! PhotoSelectorCell
        
        //Se almacena las fotos obtenidas en el arreglo
        celda.PhotoView.image = imagesArray[indexPath.item]
        
        //Retorna el dato
        return celda
    }
    
    //Se define la medida de las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width - 2) / 2
        
        //Retorna el valor
        return CGSize(width: width, height: width)
    }
    
    
    //Minimo de espacio de intermedio
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    //Minimo de lieneas de espacio entre las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        //Calcula la altuma
        let width = view.frame.width
        
        //Se define la medida del header
        return CGSize(width: width, height: width)
    }
    
    //Referencia de la foto que se muestra en el header
    var header: PicSelectorHeader?

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: header_id, for: indexPath) as! PicSelectorHeader
        
        self.header = header
        
        //Se igualan los valores para presentar la imagen seleccionada en el header
        header.PhotoView.image = ImagenSelec
        
        if let selectedImagen = ImagenSelec {
            
            //Encuentra la posicion de un elementos en particular
            if let index = self.imagesArray.firstIndex(of: selectedImagen) {
                
                //Almacena en un arreglo el elemento encontrado
                let selectAssets = self.assets[index]
                
                //Define el tamaño del assets
                let targetSize = CGSize(width: 600, height: 600)
                
                //Define metodo de descarga de elementos
                let managerImagen = PHImageManager.default()
                
                //Descarga los elementos con full medida
                managerImagen.requestImage(for: selectAssets, targetSize: targetSize, contentMode: .default, options: nil) { (images, info) in
                    //Se igualan valores
                    header.PhotoView.image = images
                }
            }
        }
        
        return header
    }
    
    //Ubica objetos en el rectangulo
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
    }
    
    //Funcion que manda a publicar la foto seleccionada
    @objc fileprivate func PostPic() {
        
        //Controlador
        let sharePicController = SharePicController()
        
        //Accede a la variable de referencia en el controlador de compartir foto
        sharePicController.selectedImagen = header?.PhotoView.image
        
        //Carga el controlador
        navigationController?.pushViewController(sharePicController, animated: true)
    }
    
    //Funcion que manda a cancelar
    @objc fileprivate func CancelaPic() {
        //Intenta cargar el home feeds
        self.dismiss(animated: true, completion: nil)
    }
    
    //Oculata la barra de estado
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

