//
//  PhotoSelectorCell.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/27/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class PhotoSelectorCell: UICollectionViewCell {
    
    //Image View para mostrar las fotos procedientes del carrete
    let PhotoView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    //Inicializa el objeto
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(PhotoView)
       //Constraint
        NSLayoutConstraint.activate([
            PhotoView.topAnchor.constraint(equalTo: topAnchor),
            PhotoView.leftAnchor.constraint(equalTo: leftAnchor),
            PhotoView.rightAnchor.constraint(equalTo: rightAnchor),
            PhotoView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
