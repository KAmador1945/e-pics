//
//  SharePicController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/4/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class SharePicController: UIViewController {
    
    //Referencia de la foto seleccionada
    var selectedImagen: UIImage? {
        didSet {
            //Carga el contenido que muestra el header
            self.PhotoViewPost.image = selectedImagen
        }
    }
    
    //Textview
    let textoView: UITextView = {
        let tv = UITextView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0), textContainer: nil)
        tv.textAlignment = NSTextAlignment.justified
        tv.font = UIFont(name: "Avenir Next", size: 15)
        tv.textColor = UIColor.black
        tv.backgroundColor = UIColor.white
        tv.layer.borderColor = UIColor.gray.cgColor
        tv.layer.borderWidth = 1
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    //Foto ImageView
    let PhotoViewPost: UIImageView = {
        let img = UIImage(named: "user_avatar.png")
        let iv = UIImageView(image: img)
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define el color de fondo
        view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)
        
        //Manda a llamar las funciones
        PreparaPostImagen()
        PreparaRightBotn()
    }
    
    //Prepara area de post e imagen
    fileprivate func PreparaPostImagen() {

        //Agrega el contenedor de vistas
        let contenedorVista = UIView()
        contenedorVista.backgroundColor = .white
        contenedorVista.layer.zPosition = 1
        contenedorVista.translatesAutoresizingMaskIntoConstraints = false

        //Se manda a agregar a la vista
        view.addSubview(contenedorVista)

        //Se manda a ubicar el objeto
        NSLayoutConstraint.activate([
            contenedorVista.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor),
            contenedorVista.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            contenedorVista.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            contenedorVista.heightAnchor.constraint(equalTo: view.heightAnchor)
        ])

        //Se agrega a la vista
        contenedorVista.addSubview(PhotoViewPost)

        //Constraint
        NSLayoutConstraint.activate([
            PhotoViewPost.topAnchor.constraint(equalTo: contenedorVista.topAnchor, constant: 5),
            PhotoViewPost.leftAnchor.constraint(equalTo: contenedorVista.leftAnchor),
            PhotoViewPost.widthAnchor.constraint(equalToConstant: 90),
            PhotoViewPost.heightAnchor.constraint(equalToConstant: 90)
        ])
        
        //Se agrega a la vista
        contenedorVista.addSubview(textoView)
        
        //Constraint
        NSLayoutConstraint.activate([
            textoView.topAnchor.constraint(equalTo: PhotoViewPost.topAnchor, constant: 0),
            textoView.leftAnchor.constraint(equalTo: PhotoViewPost.rightAnchor, constant: 3),
            textoView.rightAnchor.constraint(equalTo: contenedorVista.rightAnchor, constant: -3),
            textoView.widthAnchor.constraint(equalToConstant: contenedorVista.frame.width + PhotoViewPost.frame.width),
            textoView.heightAnchor.constraint(equalToConstant: 90)
        ])
    }
    
    //Prepara boton de post
    fileprivate func PreparaRightBotn() {
        //Agrega el constructor
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(RealizaPost))
    }
    
    //Realiza post
    @objc fileprivate func RealizaPost() {
        
        //Desactiva el boton de publicar
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        //Captura el caption a publicar
        guard let CaptionPost = textoView.text, CaptionPost.count > 0 else {
            //Activa el boton nuevamente
            navigationItem.rightBarButtonItem?.isEnabled = true
            return
        }
        
        //Obtiene el valor del dato
        guard let imagePost = selectedImagen else { return }
        
        //nombre del archivo
        let filename = UUID().uuidString
        
        //Dato a subir
        guard let DataSubir = imagePost.jpegData(compressionQuality: 0.3) else { return }
        
        //Metodo de almacenaje
        Storage.storage().reference().child("posts").child(filename).putData(DataSubir, metadata: nil) { (metadata, error) in
            
            //Detecta si ocurrio algun error
            if let err = error {
                  
                print("Oops algo va mal\(err)")
                //Ejecuta el bloque en segundo plano
                DispatchQueue.main.async {
                    //Contiene el mensaje de error
                    let mg = UILabel()
                    mg.textColor = UIColor.white
                    mg.textAlignment = .center
                    mg.font = UIFont(name: "Avenir Next", size: 17)
                    mg.text = "Oops something wrong, check and try again"
                    mg.numberOfLines = 0
                    mg.layer.zPosition = 1
                    mg.clipsToBounds = true
                    mg.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.6)
                    mg.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 8)
                    
                    //Se agrega a la vista
                    self.view.addSubview(mg)
                    
                    //Proceso de animacion
                    UIView.animate(withDuration: 1.5, delay: 3, options: .curveEaseOut, animations: {
                        mg.alpha = 0.0
                    }, completion: { (_) in
                        mg.removeFromSuperview()
                    })
                }
                
                return
            } else {
                
                //Se crea la referencia
                let StorageRef = Storage.storage().reference().child("posts").child(filename)
                
                //Metodo de descarga
                StorageRef.downloadURL(completion: { (url, error) in
                    
                    if let err = error {
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        print("Oops algo va mal", err)
                        
                        //Ejecuta el bloque en segundo plano
                        DispatchQueue.main.async {
                            //Contiene el mensaje de error
                            let mg = UILabel()
                            mg.textColor = UIColor.white
                            mg.textAlignment = .center
                            mg.font = UIFont(name: "Avenir Next", size: 17)
                            mg.text = "Oops something wrong, check and try again"
                            mg.numberOfLines = 0
                            mg.layer.zPosition = 1
                            mg.clipsToBounds = true
                            mg.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.6)
                            mg.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 8)
                            
                            //Se agrega a la vista
                            self.view.addSubview(mg)
                            
                            //Proceso de animacion
                            UIView.animate(withDuration: 1.5, delay: 3, options: .curveEaseOut, animations: {
                                mg.alpha = 0.0
                            }, completion: { (_) in
                                mg.removeFromSuperview()
                            })
                        }
                    } else {
                        
                        //Captura la url del post realizado
                        guard let fotoURL = url?.absoluteString else { return }
                        
                        //Manda a llamar la funcion que relaciona los posts con el usuario
                        self.RefPostUser(fotoURL: fotoURL)
                    }
                })
            }
        }
    }
    
    //Valor estatico para refrescar los datos
    static let nameNotify = NSNotification.Name("UpdateFeed")
    
    //Manda a guardar la imagen con url en la base de datos
    fileprivate func RefPostUser(fotoURL: String) {
        
        //Captura el id del usuario en sesion
        guard let idUser = Auth.auth().currentUser?.uid else { return }
        
        //Captura la imagen de post
        guard let postImagen = selectedImagen else { return }
        
        //Captura el caption
        guard let CaptionText = textoView.text, CaptionText.count > 0 else {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return
        }
        
        //Se crea la referencia entre el usuario y el post
        let PostUserRef = Database.database().reference().child("posts").child(idUser)
        
        //Genera child con un unico id
        let ref = PostUserRef.childByAutoId()
        
        //Fecha en tiempo real tipo UNIX
        let DatePost = Date().timeIntervalSince1970
        
        //Captura los valores a publicar
        let valores = ["fotoURL":fotoURL, "caption":CaptionText, "imagenWidth":postImagen.size.width, "imagenHeight":postImagen.size.height, "date": DatePost] as [String:Any]
        
        //Metodo Firebase para actualiza la base de datos
        ref.updateChildValues(valores) { (err, ref) in
            //Detecta si ocurrio algun error
            if let err = err {
                print("Oops algo va mal", err)
            }
            
            //Mensaje
            print("Posts guardado exitosamente")
            
            //Cierra el controlador de posts
            self.dismiss(animated: true, completion: nil)
            
            //Notifica la entrega al Home Feed
            NotificationCenter.default.post(name: SharePicController.nameNotify, object: nil)
        }
    }
    
    //Oculata el teclado con tocar en la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    //Oculta la barra de estado
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
