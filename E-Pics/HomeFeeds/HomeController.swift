//
//  HomeController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/10/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HomePostCellDelegate {
    
    //Global References
    var homePostCell: HomePostCell?
    
    //Access data user
    var user: User?

    //Cell id
    let celdaID = "celdaID"
    
    //UIView when do not exist data.
    let View = UIView()
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Manda a actualizar el homefeed automaticamente
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateFeed), name: SharePicController.nameNotify, object: nil)
        
        //Color de fondo
        collectionView.backgroundColor = .white
        
        //Se registra la clase
        collectionView.register(HomePostCell.self, forCellWithReuseIdentifier: celdaID)
        
        //Refresh controller
        let RefreshControllers = UIRefreshControl()
        RefreshControllers.addTarget(self, action: #selector(ControlaRefresh), for: .valueChanged)
        
        //Checha si esta disponible
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = RefreshControllers
        } else {
            collectionView.addSubview(RefreshControllers)
        }
        
        //Prepara los items de navegacion
        PreparaItemsNav()
        
        //Descarga todos los posts
        DescargaAllPost()
    }
    
    //Actualiza el homefeed automaticamente
    @objc fileprivate func UpdateFeed() {
        ControlaRefresh()
    }
    
    //Manda a hacer refresh al controlador
    @objc fileprivate func ControlaRefresh() {
        print("Refrescando informacion")
        DescargaAllPost()
        postUsr.removeAll()
    }
    
    fileprivate func DescargaAllPost() {
        DescargaPostUsr()
        DescargaIdUsrsFollows()
    }
    
    
    //Descarga id de los usuarios que sigue el usuario en sesion
    fileprivate func DescargaIdUsrsFollows() {
        
        //Descarga los id de los usuarios que esta siendo seguidos por el usuario en sesion
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        //Metodo de descarga
        Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            print(snapshot.value as Any)
            
            //Crea un diccionario
            guard let UserIdDiccionario = snapshot.value as? [String:Any] else { return }
            
            //Recorre los valores del diccionario
            UserIdDiccionario.forEach({ (data) in
                
                //Almacena la clave y el valor
                let (key,_) = data
                
                //Metodo para descargar posts del usuario por medio de su id
                Database.DescargaUsrConUID(uid: key, completion: { (user) in
                    self.DescargaPostConUsr(user: user)
                })
            })
            
        }) { (error) in
            print("Ocurrio un error al descargar los id de los usuarios a seguir", error)
        }
    }
    
    //Para iOS 10 > 
    let RefreshController = UIRefreshControl()
    
    //Variable global
    var postUsr = [PostUsr]()
    
    //Manda a descargar los post con el usuario
    fileprivate func DescargaPostConUsr(user: User) {
        
        //Se crea una referencia a la base de datos
        let ref = Database.database().reference().child("posts").child(user.uid)
        
        //Manda a descargar
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            //Finaliza el refresh data
            if #available(iOS 10.0, *) {
                self.collectionView.refreshControl?.endRefreshing()
            } else {
                self.collectionView.addSubview(self.RefreshController)
            }
            
            
            //Se manda a crear un diccionario
            guard let PostDiccionario = snapshot.value as? [String: Any] else { return }
            
            //Manda a recorrer los datos del diccionario
            PostDiccionario.forEach({ (data) in
                
                //Almacena los datos
                let (key, value) = data
                
                //Se manda a crear el diccionario
                guard let diccionario = value as? [String:Any] else { return }
                
                //Contiene el diccionario
                var post = PostUsr(user: user, diccionarioPost: diccionario)
                
                //Se almacena los datos
                post.id = key
                
                //Accede al id del usuario
                guard let uid = Auth.auth().currentUser?.uid else { return }
                
                //Accede al nodo
                let ref = Database.database().reference().child("unlikes").child(key).child(uid)

                //Realiza la descarga de la informacion
                ref.observeSingleEvent(of: .value, with: { (snapshot) in

                    //Compara valores para determinar si es unlike o no
                    if let data = snapshot.value as? Int, data != 1 {
                        post.noLiked = true
                    } else {
                        post.noLiked = false
                    }
                })
                
                //Manda a descargar los likes
                Database.database().reference().child("likes").child(key).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let values = snapshot.value as? Int, values == 1 {
                        post.hasLiked = true
                    } else {
                        post.hasLiked = false
                    }
                    
                    //Mando a insertar los datos en el arreglo
                    self.postUsr.append(post)
                    
                    //Ordena los posts por medio de fecha
                    self.postUsr.sort(by: { (p1, p2) -> Bool in
                        return p1.postDate.compare(p2.postDate) == .orderedDescending
                    })
                    
                    //Proceso en segundo plano
                    DispatchQueue.main.async {
                        self.collectionView?.reloadData()
                    }
                })
            })
            
        }) { (error) in
            print("Oops algo ocurrio al descargar los posts realizados", error)
        }
    }
    
    //Prepara los items de navegacion
    fileprivate func PreparaItemsNav() {
        
        //Centro de la marca
        let logo = UIImage(named: "MarcaNav.png")
        navigationItem.titleView = UIImageView(image: logo)
        
        //Camara lado izquierdo
        guard let camaraLogo = UIImage(named: "camera.png") else { return }
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: camaraLogo.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(ControlaCamara))
    }
    
    //Controla la camara
    @objc fileprivate func ControlaCamara() {
        print("Abriendo camara")
        
        //Accede al controlador
        if #available(iOS 10.0, *) {
            
            //Accede al controlador de la camara
            let cameraController = CameraController()
            
            //Muestra el controlador
            present(cameraController, animated: true, completion: nil)
        }
    }
    
    //Valor estatico para refrescar los datos
    static let updateProfile = NSNotification.Name("UpdateProfile")
    
    //Manda a descargar los posts realizados por el usuario
    fileprivate func DescargaPostUsr() {
        
        //Captura el id del usuario
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        //Accede al metodo
        Database.DescargaUsrConUID(uid: uid) { (user) in
            self.DescargaPostConUsr(user: user)
            
            NotificationCenter.default.post(name: HomeController.updateProfile, object: nil)
        }
    }
    
    //Define el numero de items que tendra
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //Asegura que el arreglo no este vacio
        if !postUsr.isEmpty {
            DispatchQueue.main.async {
                self.View.removeFromSuperview()
            }
            return postUsr.count
        } else {
            MessageDoNotData()
        }
        
        return 0
    }
    
    //Create and set objects when do not exist data from server
    func MessageDoNotData() {
        
        View.translatesAutoresizingMaskIntoConstraints = false
        View.backgroundColor = UIColor.purple
        View.layer.cornerRadius = 130 / 2
        View.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.View.transform = .identity
        }, completion: nil)
        
        self.collectionView.addSubview(View)
        
        NSLayoutConstraint.activate([
            View.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            View.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            View.widthAnchor.constraint(equalToConstant: 130),
            View.heightAnchor.constraint(equalToConstant: 130),
        ])
        
    }
    
    //Se crea las celdas para cada items creado
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: celdaID, for: indexPath) as! HomePostCell
        
        //Accede al homeController desde la celda
        celda.homeController = self

        //Agrega los items a cada una de las celdas
        if indexPath.item < postUsr.count {
            celda.posts = postUsr[indexPath.item]
        }
        
        //Accede al delegate
        celda.delegate = self
        
        return celda
    }
    
    //Funcion de seleccion de comentarios
    func CommentSelecTap(post: PostUsr) {
        
        //Accede al controlador
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        navigationController?.pushViewController(commentsController, animated: true)
    }
    
    
    //Define la medida de las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Define la altura
        var height: CGFloat = 40 + 8 + 8
        height += view.frame.width
        height += 60
        height += 60
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //Referencia al frame
    var startingFrame: CGRect?
    var backgroundBlack: UIView?
    
    //Custom zoom
    func ZoomImageView(StartingimagenView: UIImageView) {
        
        //Inicializa el imageView como un frame
        startingFrame = StartingimagenView.superview?.convert(StartingimagenView.frame, from: nil)
        
        //Se manda a construir un bloque de ImagenView
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.translatesAutoresizingMaskIntoConstraints = false
        
        //Se pasa la imagen al frame
        zoomingImageView.image = StartingimagenView.image
        zoomingImageView.contentMode = .scaleAspectFit //.scaleAspectFill //.scaleToFill //.scaleAspectFit
        zoomingImageView.clipsToBounds = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ControlaZoomOut)))
        zoomingImageView.isUserInteractionEnabled = true
        
        //Se manda a agregar a la vista
        if let keyWindows = UIApplication.shared.keyWindow {
            
            //Color de fondo negro
            backgroundBlack = UIView(frame: keyWindows.frame)
            backgroundBlack!.backgroundColor = UIColor.black
            backgroundBlack!.alpha = 0
            
            //Se agrega a la vista
            keyWindows.addSubview(self.backgroundBlack!)
            keyWindows.addSubview(zoomingImageView)

            //Define los Anchors
            zoomingImageView.widthAnchor.constraint(equalToConstant: keyWindows.frame.width).isActive = true
            zoomingImageView.heightAnchor.constraint(equalToConstant: keyWindows.frame.height).isActive = true
            
            //Se realiza la animacion
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                
                //Visible background
                self.backgroundBlack!.alpha = 1
                
                //Define el ancho y alto del frame
//                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindows.frame.width
//                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindows.frame.width, height: height)
                
                //Centra el objeto
                zoomingImageView.centerYAnchor.constraint(equalTo: keyWindows.centerYAnchor).isActive = true
                zoomingImageView.centerXAnchor.constraint(equalTo: keyWindows.centerXAnchor).isActive = true
                
            }, completion: nil)
        }
    }

    //Controla el regreso del Zooming
    @objc func ControlaZoomOut(TapReconizer: UITapGestureRecognizer) {
        
        //Detecta el tap sobre la vista
        guard let zoomingOut = TapReconizer.view else { return }
        zoomingOut.translatesAutoresizingMaskIntoConstraints = false
        backgroundBlack?.translatesAutoresizingMaskIntoConstraints = false
        
        //Realiza animacion
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            
            //Devuelve los valores iniciales al frame
            zoomingOut.frame = self.startingFrame!
            zoomingOut.widthAnchor.constraint(equalToConstant: 0).isActive = true
            zoomingOut.heightAnchor.constraint(equalToConstant: 0).isActive = true
            
            //Dismiss
            self.backgroundBlack?.widthAnchor.constraint(equalToConstant: 0).isActive = true
            self.backgroundBlack?.heightAnchor.constraint(equalToConstant: 0).isActive = true
            
            //Detecta si finaliza la animacion
        }) { (complete:Bool) in
            //Remueve el frame de la super vista
            zoomingOut.removeFromSuperview()
        }
    }
    
    //Controla boton de like
    func LikeTaped(for cell: HomePostCell) {
        
        //Accede a los datos de la celda
        guard let IndexPath = collectionView.indexPath(for: cell) else { return }
        
        //Accede al post para dar like
        var post = self.postUsr[IndexPath.item]
        
        //Obtiene el id del post
        guard let postId = post.id else { return }
        
        //Accede al id del usuario
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        //Crea referencia a la base de datos
        let ref = Database.database().reference().child("likes").child(postId)
        
        //Define los valores a insertar
        let values = [uid: post.hasLiked == true ? 0: 1]
        
        //Manda a crear la columna de like
        ref.updateChildValues(values) { (err, ref) in
            
            if let error = err {
                print("Oops algo va mal", error)
            } else {
                print("Te ha gustado el post")
        
                //Ejecuta el bloque de codigo de manera asyncrona
                DispatchQueue.main.async {
                    post.hasLiked = !post.hasLiked
                    self.postUsr[IndexPath.item] = post
                    self.collectionView?.reloadItems(at: [IndexPath])
                }
            }
        }
     }
    
    //Controla el boton de no like
    func UnLikeTaped(for cell: HomePostCell) {
        
        //Accede a los datos de la celda
        guard let IndexPath = collectionView.indexPath(for: cell) else { return }
        
        //Accede al post dar unlike
        var post = self.postUsr[IndexPath.item]
        
        //Obtiene el id del post
        guard let postId = post.id else { return }
        
        //Accede al id del usuario en sesion
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        //Crea la referencia a la base de datos
        let ref = Database.database().reference().child("unlikes").child(postId)
        
        //Se define el valor a insertar
        let values = [uid: post.noLiked == true ? 1: 0]
        
        //Manda a crear la columna de unlikes
        ref.updateChildValues(values) { (error, ref) in
            //Detecta si ocurre algun error
            if let err = error {
                print("Oops no se logro dar Unlike", err)
            } else {
                print("No te ha gustado el post")
                
                //Ejecuta el bloque de codigo de manera asyncrona
                DispatchQueue.main.async {
                    post.noLiked = !post.noLiked
                    self.postUsr[IndexPath.item] = post
                    self.collectionView?.reloadItems(at: [IndexPath])
                }
            }
        }
    }
    
    //Delete post from user
    func DeleteSelectPost(post: PostUsr) {
        
        //id user
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        //Accede al id del post
        guard let postId = post.id else { return }
        
        //Define icon
        let iconTrash = UIImage(named: "trash.png")
        
        //Custom title
        
        
        //Alert Controller
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let ref = Database.database().reference().child("posts").child(uid).child(postId)
        
      
        //Define action
        let action = UIAlertAction(title: "Delete this post", style: .destructive) { (_) in
            
            print("Eliminado post...\(postId)")
            
            //Method for delete post
            ref.removeValue(completionBlock: { (err, ref) in
                
                if let error = err {
                    print("No se logro eliminar el posts: \(error)")
                } else {
                    print("Se logro eliminar el posts")
                    
                }
            })
            
            //Execute this code block in background
            DispatchQueue.main.async {
                if let collec = self.collectionView {
                    collec.reloadData()
                    self.postUsr.removeAll()
                    self.DescargaAllPost()
                }
            }
        }
        
        //Define icon for alert
        action.setValue(0, forKey: "titleTextAlignment")
        action.setValue(iconTrash, forKey: "image")
        
        //Add Action to alert
        alertController.addAction(action)
        
        //Crea boton de cancelar
        let cancelbtn = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelbtn)
        
        //Muestra el controlador
        present(alertController, animated: true, completion: nil)
    }
}
