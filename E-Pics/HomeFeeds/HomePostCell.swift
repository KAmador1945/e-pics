//
//  HomePostCell.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/10/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

protocol HomePostCellDelegate {
    
    //Define el parametro de post
    func CommentSelecTap(post: PostUsr)
    func DeleteSelectPost(post: PostUsr)
    func LikeTaped(for cell: HomePostCell)
    func UnLikeTaped(for cell: HomePostCell)
    
}

class HomePostCell: UICollectionViewCell, UIScrollViewDelegate {
    
    var homeController: HomeController?

    //Se manda a crear un delegate
    var delegate: HomePostCellDelegate?
    
    var posts: PostUsr? {
        
        didSet {

            //Captura la url de la foto
            guard let fotoURL = self.posts?.fotoURL else { return }
            
            //Descarga las fotos publicadas
            FotoImageView.CargaImagen(urlString: fotoURL)
            
            //Username
            UsernameLbl.text = posts?.user.username
            
            //Fecha de publicacion
            let dateAgo = posts?.postDate.TimeAgoDisplay()
            DateLbl.text = dateAgo
            
            //Foto de Perfil
            guard let AvatarPerfil = posts?.user.fotoPerfilURL else { return }
            Avatar.CargaImagen(urlString: AvatarPerfil)
            
            //Cambia de imagen el boton para indicar que fue presionado
            likeBtn.setImage(posts?.hasLiked == true ? #imageLiteral(resourceName: "like_selected.png").withRenderingMode(.alwaysOriginal) : #imageLiteral(resourceName: "like_unselected.png").withRenderingMode(.alwaysOriginal), for: .normal)
            
            UnlikeBtn.setImage(posts?.noLiked == true ? #imageLiteral(resourceName: "selected_unlike.png").withRenderingMode(.alwaysOriginal) : #imageLiteral(resourceName: "unselected_unlike.png").withRenderingMode(.alwaysOriginal), for: .normal)
            
            //Manda a llamar las funciones
            UnlikeHiddenBtn()
            PreparaCaptionsPosts()
            OptionHiddenBtn()
        }
    }
    
    //Carga Avatar
    let Avatar: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        let img = CustumImagenView(image: ava)
        img.layer.cornerRadius = 50 / 2
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.layer.borderColor = UIColor.lightGray.cgColor
        img.layer.borderWidth = 1
        img.layer.zPosition = 1
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //ImageView del home
    lazy var FotoImageView: CustumImagenView = {
        let img = CustumImagenView()
        img.backgroundColor = .white
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ControlaZoomIn)))
        img.isUserInteractionEnabled = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Username label
    let UsernameLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "Username"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 20)
        lbl.layer.zPosition = 1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let DateLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.lightGray
        lbl.text = "1 week ago"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 13)
        lbl.layer.zPosition = 1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    
    //Boton de opciones
    lazy var OpcionBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("•••", for: .normal)
        btn.setTitleColor(.gray, for: .normal)
        btn.addTarget(self, action: #selector(OpcionePost), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        //btn.isHidden = true
        return btn
    }()
    
    //Botones del footer de la celda
    lazy var likeBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "like_unselected")
        btn.addTarget(self, action: #selector(ManejaLike), for: .touchUpInside)
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    
    lazy var UnlikeBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "unselected_unlike")
        btn.addTarget(self, action: #selector(ManejaUnLike), for: .touchUpInside)
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    
    lazy var CommentsBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "comments")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(ManejaComments), for: .touchUpInside)
        return btn
    }()
    
    lazy var ShareBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "share")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        return btn
    }()
    
    let CaptionView: UITextView = {
        let tv = UITextView()
        tv.isScrollEnabled = false
        tv.isEditable = false
        tv.textAlignment = .center
        tv.backgroundColor = .white
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    //Se inicializa
    override init(frame: CGRect) {
        super.init(frame: frame)
        
       
        
        //Color de fondo
        backgroundColor = .white
        
        //Prepara ubicacion de fotos
        PreparaDatosUsr()
        PreparaBtnAccions()
        PreparaCaptions()
    }
    
    //Prepara los datos del usuario a mostrar en la celda
    fileprivate func PreparaDatosUsr() {
        
        //Manda a agregar a la foto de perfil
        addSubview(Avatar)
        addSubview(UsernameLbl)
        addSubview(DateLbl)
        addSubview(OpcionBtn)
        addSubview(FotoImageView)
        addSubview(CaptionView)
        
        //Constraint
        NSLayoutConstraint.activate([
            Avatar.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            Avatar.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            Avatar.widthAnchor.constraint(equalToConstant: 50),
            Avatar.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            UsernameLbl.topAnchor.constraint(equalTo: Avatar.topAnchor, constant: 3),
            UsernameLbl.leftAnchor.constraint(equalTo: Avatar.leftAnchor, constant: 60)
        ])
        
        NSLayoutConstraint.activate([
            DateLbl.topAnchor.constraint(equalTo: UsernameLbl.bottomAnchor, constant: 0),
            DateLbl.leftAnchor.constraint(equalTo: UsernameLbl.leftAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            OpcionBtn.topAnchor.constraint(equalTo: Avatar.topAnchor, constant: 8),
            OpcionBtn.rightAnchor.constraint(equalTo: rightAnchor, constant: -8)
        ])
        
        //Constraints
        NSLayoutConstraint.activate([
            FotoImageView.topAnchor.constraint(equalTo: Avatar.topAnchor, constant: 60),
            FotoImageView.leftAnchor.constraint(equalTo: leftAnchor),
            FotoImageView.rightAnchor.constraint(equalTo: rightAnchor),
            FotoImageView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1)
        ])
    }
    
    //Prepara las funciones de los botones en el footer
    fileprivate func PreparaBtnAccions() {
        
        //Contenedor de pila
        let stackview = UIStackView(arrangedSubviews: [likeBtn, UnlikeBtn, CommentsBtn, ShareBtn])
        
        //Config stackview
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.translatesAutoresizingMaskIntoConstraints = false
        
        //Se agrega a la vista
        addSubview(stackview)
        
        //Constraint
        NSLayoutConstraint.activate([
            stackview.topAnchor.constraint(equalTo: FotoImageView.bottomAnchor),
            stackview.leftAnchor.constraint(equalTo: leftAnchor, constant: 4),
            stackview.rightAnchor.constraint(equalTo: rightAnchor, constant: -200),
            stackview.heightAnchor.constraint(equalToConstant: 50)
        ])
        
    }
    
    //Prepara captions
    fileprivate func PreparaCaptions() {
        
        //Constraint
        NSLayoutConstraint.activate([
            CaptionView.topAnchor.constraint(equalTo: likeBtn.bottomAnchor, constant: 0),
            CaptionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            CaptionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0)
        ])
    }
    
    //Manda a descargar el caption y muestra el valor en el objeto
    fileprivate func PreparaCaptionsPosts() {
        
        //accede al post
        guard let post = self.posts else { return }
        
        //Texto atributo
        let atributoText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.black])
        atributoText.append(NSAttributedString(string: " \(post.captionPost)", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black]))
        atributoText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 4)]))
        
        //Agrega los datos al objeto
        CaptionView.attributedText = atributoText
    }
    
    //Controla los like
    @objc fileprivate func ManejaLike() {
        delegate?.LikeTaped(for: self)
    }
    
    //Controla los no like
    @objc fileprivate func ManejaUnLike() {
        delegate?.UnLikeTaped(for: self)
    }
    
    //Oculta el boton de unlike si para sus propios posts
    func UnlikeHiddenBtn() {
        
        //Captura el id del usuarios quien realizo el post
        guard let IdUserPost = posts?.user.uid else { return }
        
        //Captura el id del ususario actual en sesion
        guard let CurrentUserId = Auth.auth().currentUser?.uid else { return }
        
        //Compara el id del usuario actual con el id del usuario quien creo el post
        if CurrentUserId == IdUserPost {
            UnlikeBtn.isHidden = true
        } else {
            UnlikeBtn.isHidden = false
        }
    }

    //Prepara las acciones sobre los posts
    @objc fileprivate func OpcionePost() {
        guard let post = self.posts else { return }
        delegate?.DeleteSelectPost(post: post)
        
    }
    
    //Oculta el boton de opciones
    func OptionHiddenBtn() {
        
        guard let idUserPost = posts?.user.uid else { return }
        guard let CurrentUsrId = Auth.auth().currentUser?.uid else { return }
        
        if CurrentUsrId == idUserPost {
            self.OpcionBtn.isHidden = false
        } else {
            self.OpcionBtn.isHidden = true
        }
    }
    
    //Carga el controlador de comentarios
    @objc fileprivate func ManejaComments() {
        print("Comentando...")
        
        //Almacena los posts realizado por el usuario
        guard let post = self.posts else { return }
        
        //Accede al protocolo
        delegate?.CommentSelecTap(post: post)
    }
    
    //Detecta si se realizo tap sobre la imagen
    @objc func ControlaZoomIn(tapGesture: UITapGestureRecognizer) {
        
        //Se guarda el gesto hacia la imagen
        guard let imagenView = tapGesture.view as? UIImageView else { return }
        
        //Accede a la funcion que contiene el zoom
        self.homeController?.ZoomImageView(StartingimagenView: imagenView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
