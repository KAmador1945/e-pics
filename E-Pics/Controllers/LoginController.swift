//
//  LoginController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/13/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController, UITextFieldDelegate {

    //Titulo del Apps
    let Marca: UIImageView = {
        let brand = UIImage(named: "Marca.png")
        let img = UIImageView(image: brand)
        img.contentMode = .scaleAspectFill
        img.layer.zPosition = 1
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Textfield email
    let email: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Email"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.autocapitalizationType = .none
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    
    //Textfield Password
    let password: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Password"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.isSecureTextEntry = true
        txt.autocapitalizationType = .none
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    //Boton de iniciar sesion
    let LoginBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Sign In", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 17)
        btn.layer.cornerRadius = 4
        btn.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.7)
        btn.addTarget(self, action: #selector(SignIn), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.zPosition = 1
        btn.isEnabled = false
        return btn
    }()
    
    //Boton de registro
    let RegisterBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.addTarget(self, action: #selector(Carga), for: .touchUpInside)
        let textAtributo = NSMutableAttributedString(string: "Don't ready?", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 16)!, NSAttributedString.Key.foregroundColor: UIColor.white])
        textAtributo.append(NSAttributedString(string: " Register!", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 16)!, NSAttributedString.Key.foregroundColor: UIColor.white]))
        btn.setAttributedTitle(textAtributo, for: .normal)
        btn.layer.zPosition = 1
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Derecho de autor
    let DerechoAutor: UILabel = {
        let lbl = UILabel()
        lbl.text = "Standarte Software, All right reserved 2019."
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.layer.zPosition = 1
        return lbl
    }()
    
    //Capa de grandiente
    var grandientLayer: CAGradientLayer!
    var colorsSet = [[CGColor]]()
    var colorActual: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Oculta la barra de navegacion
        navigationController?.isNavigationBarHidden = true
        
        //Delegates
        email.delegate = self
        email.tag = 0
        password.delegate = self
        password.tag = 1
        
        //Manda a llamar la funcion
        UbicaMarca()
        PreparaInputs()
        SetCopyRight()
        CreaGradientLayer()
    }
    
    //Manda a ubicar la marca
    fileprivate func UbicaMarca() {
        view.addSubview(Marca)
        
        //Manda a ubicar
        NSLayoutConstraint.activate([
            Marca.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            Marca.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 8),
        ])
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Prepara inputs
    fileprivate func PreparaInputs() {
        //StackView
        let stackview = UIStackView(arrangedSubviews: [email, password])
        
        //Distribucion
        stackview.distribution = .fillEqually
        stackview.axis = .vertical
        stackview.spacing = 7
        
        //Se manda a agregar a la vista
        view.addSubview(stackview)
        stackview.layer.zPosition = 1
        stackview.translatesAutoresizingMaskIntoConstraints = false
        
        //Ubica el stackview
        NSLayoutConstraint.activate([
            stackview.topAnchor.constraint(equalTo: Marca.bottomAnchor, constant: view.frame.height / 7),
            stackview.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 35),
            stackview.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -35),
            stackview.heightAnchor.constraint(equalToConstant: 90)
        ])
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    
        //Manda a agregar el boton de login
        view.addSubview(LoginBtn)
        view.addSubview(RegisterBtn)
        
        //Manda a ubicar el objeto
        NSLayoutConstraint.activate([
            LoginBtn.topAnchor.constraint(equalTo: stackview.bottomAnchor, constant: 20),
            LoginBtn.leftAnchor.constraint(equalTo: stackview.leftAnchor, constant: 0),
            LoginBtn.rightAnchor.constraint(equalTo: stackview.rightAnchor, constant: 0),
            LoginBtn.centerXAnchor.constraint(equalTo: stackview.centerXAnchor),
        ])
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
        
        //Manda a agregar el boton de crear cuenta
        NSLayoutConstraint.activate([
            RegisterBtn.topAnchor.constraint(equalTo: LoginBtn.bottomAnchor, constant: 30),
            RegisterBtn.centerXAnchor.constraint(equalTo: LoginBtn.centerXAnchor),
        ])
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Manda a iniciar sesion
    @objc func SignIn() {
        
        //Captura los datos
        guard let email = email.text else { return }
        guard let password = password.text else { return }
        
        //Metodo firebase para iniciar sesion
        Auth.auth().signIn(withEmail: email, password: password) { (User, error) in
            
            //Comprueba si ocurrio un error
            if let err = error {
                
                print("Oops algo va mal con el inicio de sesion", err)
                
                DispatchQueue.main.async {
                    
                    //Contiene el mensaje de error
                    let mg = UILabel()
                    mg.textColor = UIColor.white
                    mg.textAlignment = .center
                    mg.font = UIFont(name: "Avenir Next", size: 17)
                    mg.text = "Oops something went wrong!"
                    mg.numberOfLines = 0
                    mg.layer.zPosition = 1
                    mg.clipsToBounds = true
                    mg.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.6)
                    mg.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 8)
                    
                    //Se agrega a la vista
                    self.view.addSubview(mg)
                    
                    //Proceso de animacion
                    UIView.animate(withDuration: 1.5, delay: 3, options: .curveEaseOut, animations: {
                        mg.alpha = 0.0
                    }, completion: { (_) in
                        mg.removeFromSuperview()
                    })
                }
                
            } else {
                
                //Muestra el id de l usuario en sesion
                print("Este es el id del usuario identificado", User?.user.uid ?? "")
                
                //Manda a cargar el MaintabBar quien es el controlador raiz de la red
                guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {
                    return
                }
                //Accede a la funcion que prepara los controladores de cada pantalla
                mainTabBarController.PreparaVControllers()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //Manda a colocar los derecho de autor
    fileprivate func SetCopyRight() {
        
        view.addSubview(DerechoAutor)
        DerechoAutor.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        DerechoAutor.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        DerechoAutor.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Inicializa y establece valores para CAGradient
    fileprivate func CreaGradientLayer() {
        //Se inicializa
        grandientLayer = CAGradientLayer()
        
        //Se definen los limites en la vista
        grandientLayer.frame = self.view.bounds
        
        //Se mandan a elegir los colores
        grandientLayer.colors = [UIColor.purple.cgColor, UIColor.white.cgColor]
        
        //Ubicacion del gradiente
        grandientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        grandientLayer.endPoint = CGPoint(x: 0.5, y: -0.25)
        
        //Se manda a agregar a la vista una subcapa
        self.view.layer.addSublayer(grandientLayer)
    }
    
    //Controla la entrada de texto
    @objc fileprivate func CntrlaTextoInput() {
        
        let textFields = email.text?.count ?? 0 > 0 && password.text?.count ?? 0 > 0
        
        //Valida si lo campos poseen datos
        if textFields {
            LoginBtn.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.4)
        } else {
            LoginBtn.isEnabled = true
            LoginBtn.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.7)
        }
    }
    
    //Habilita el boton de return para cambiar de textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Intenta encontrar el siguiente input
        if let nextField = password.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            next?.resignFirstResponder()
        }
        
        return false
    }
    
    
    //Manda a cargar la vista de Registro
    @objc func Carga() {
        //Controlador
        let registerScreen = RegisterController()
        navigationController?.pushViewController(registerScreen, animated: true)
    }
    
    //Manda a esconder el teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Cambia el estilo de la barra de estado
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
