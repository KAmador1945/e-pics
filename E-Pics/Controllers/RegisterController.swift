//
//  RegisterController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 12/17/18.
//  Copyright © 2018 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class RegisterController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Avatar user
    let Avataruser: UIButton = {
        let boton = UIButton()
        let img = UIImage(named: "user_avatar.png")
        boton.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        boton.addTarget(self, action: #selector(AgregarFotoPerfil), for: .touchUpInside)
        boton.translatesAutoresizingMaskIntoConstraints = false
        boton.contentMode = .scaleAspectFill
        boton.layer.cornerRadius = 130 / 2
        boton.layer.borderWidth = 5
        boton.layer.borderColor = UIColor.white.cgColor
        boton.layer.zPosition = 1
        boton.clipsToBounds = false
        boton.layer.masksToBounds = true
        return boton
    }()
    
    //Textfields para capturar los datos
    let nombres: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Name"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.layer.zPosition = 1
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    let apellidos: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Lastname"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.tag = 1
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    let username: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Username"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.autocapitalizationType = .none
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    let email: UITextField = {
       let txt = UITextField()
        txt.placeholder = "Email"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.autocapitalizationType = .none
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    let password: UITextField = {
        let txt = UITextField()
        txt.placeholder = "Password"
        txt.backgroundColor = UIColor(white: 255, alpha: 0.6)
        txt.borderStyle = .roundedRect
        txt.font = UIFont(name: "Avenir Next", size: 15)
        txt.textColor = UIColor.purple
        txt.isSecureTextEntry = true
        txt.autocapitalizationType = .none
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.addTarget(self, action: #selector(CntrlaTextoInput), for: .editingChanged)
        return txt
    }()
    
    //Boton de inicio de crear cuenta
    let RegistarBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Sign Up", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 17)
        btn.setTitleColor(.white, for: .normal)
        btn.layer.cornerRadius = 4
        btn.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.7)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.zPosition = 1
        btn.addTarget(self, action: #selector(CrearCuenta), for: .touchUpInside)
        btn.isEnabled = false
        return btn
    }()
    
    //Mensaje de tienes cuenta? Inicia sesion
    let LoginBtn: UIButton = {
        let btn = UIButton(type: .system)
        let TextoAtributo = NSMutableAttributedString(string: "Are you ready?", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.white])
        TextoAtributo.append(NSAttributedString(string: " Sign In", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.white]))
        btn.setAttributedTitle(TextoAtributo, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(SignIn), for: .touchUpInside)
        btn.layer.zPosition = 1
        return btn
    }()
    
    //Derecho de autor
    let DerechoAutor: UILabel = {
        let lbl = UILabel()
        lbl.text = "Standarte Software, All right reserved 2019."
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 13)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.layer.zPosition = 1
        return lbl
    }()
    
    //Capa de grandiente
    var grandientLayer: CAGradientLayer!
    var colorsSet = [[CGColor]]()
    var colorActual: Int!
    
    //Primera funcion en inciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Oculta la barra de navegacion
        navigationController?.isNavigationBarHidden = true
        
        //Responde a las entradas enviadas por la Apps
        nombres.delegate = self
        nombres.tag = 0
        apellidos.delegate = self
        apellidos.tag = 1
        username.delegate = self
        username.tag = 2
        email.delegate = self
        email.tag = 3
        password.delegate = self
        password.tag = 4
        
        
        //Manda a cargar la funcion
        SetAvatar()
        AnimaAvatarObj()
        PreparaInputs()
        SetCopyRight()
        CreaGradientLayer()
    }
    
    //Manda a colocar el vatar
    fileprivate func SetAvatar() {
        
        //Agregando a la vista
        view.addSubview(Avataruser)
        
        //COnstraints
        Avataruser.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.width / 10).isActive = true
        Avataruser.center.x -= view.bounds.width
        Avataruser.widthAnchor.constraint(equalToConstant: 130).isActive = true
        Avataruser.heightAnchor.constraint(equalToConstant: 130).isActive = true
        
    }
    
    //Animacion de prueba
    func AnimaAvatarObj() {
        
        //Se define el constraint
        self.Avataruser.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Manda a colocar el contenedor de inputs
    fileprivate func PreparaInputs() {
        
        //Se manda a crear el stackview
        let stackView = UIStackView(arrangedSubviews: [nombres,apellidos,username,email,password])
        
        //Se manda a distribuir el stackView
        stackView.distribution = .fillEqually
        
        //Se manda a dar el sentido de ubicacion
        stackView.axis = .vertical
        stackView.spacing = 10
        
        //Se manda a agregar a la vista
        view.addSubview(stackView)
        stackView.layer.zPosition = 1
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        //Arreglo que manda  ubicar el stackview
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 35),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -35),
            stackView.heightAnchor.constraint(equalToConstant: 230)
        ])
        
        //Anima la entrada del layout
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
        
        
        //Se manda a agregar el boton a la vista
        view.addSubview(RegistarBtn)
        
        //Se manda a ubicar el boton de crear cuenta
        NSLayoutConstraint.activate([
            RegistarBtn.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 30),
            RegistarBtn.leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: 50),
            RegistarBtn.rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: -50)
        ])
        
        //Se manda a agregar a la vista
        view.addSubview(LoginBtn)
        
        //Manda a ubicar el boton que carga el login screen
        NSLayoutConstraint.activate([
            LoginBtn.topAnchor.constraint(equalTo: RegistarBtn.bottomAnchor, constant: 20),
            LoginBtn.leftAnchor.constraint(equalTo: stackView.leftAnchor, constant: 20),
            LoginBtn.rightAnchor.constraint(equalTo: stackView.rightAnchor, constant: -20)
        ])
        
        //Anima la entrada del boton
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Manda a colocar los derecho de autor
    fileprivate func SetCopyRight() {
        view.addSubview(DerechoAutor)
        DerechoAutor.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        DerechoAutor.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        DerechoAutor.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        //Anima el constraint
        UIView.animate(withDuration: 0.9) {
            self.view.layoutIfNeeded()
        }
    }
    
    //Inicializa y establece valores para CAGradient
    fileprivate func CreaGradientLayer() {
        
        //Se inicializa
        grandientLayer = CAGradientLayer()
        
        //Se definen los limites en la vista
        grandientLayer.frame = self.view.bounds
        
        //Se mandan a elegir los colores
        grandientLayer.colors = [UIColor.purple.cgColor, UIColor.white.cgColor]

        //Ubicacion del gradiente
        grandientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        grandientLayer.endPoint = CGPoint(x: 0.5, y: -0.25)
        
        //Se manda a agregar a la vista una subcapa
        self.view.layer.addSublayer(grandientLayer)
    }
    
    //Manda a seleccionar la foto
    @objc fileprivate func AgregarFotoPerfil() {
        print("Seleccionando foto...")
        //Accede al carrete
        let imagenPickerController = UIImagePickerController()
        imagenPickerController.delegate = self
        imagenPickerController.allowsEditing = true
        present(imagenPickerController, animated: true, completion: nil)
    }
    
    //Obtiene la informacion si la foto fue editada o no
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //Detecta si la foto no fue editada
        if let FotoEditada = info[.editedImage] as? UIImage {
            
            //Se manda a insertar la foto
            Avataruser.setImage(FotoEditada.withRenderingMode(.alwaysOriginal), for: .normal)
            
        } else if let FotoOriginal = info[.originalImage] as? UIImage {
            
            //Se manda a insertar la foto en el btn
            Avataruser.setImage(FotoOriginal.withRenderingMode(.alwaysOriginal), for: .normal)
            
        }
        
        //Remueve el carrete
        dismiss(animated: true, completion: nil)
    }
    
    //Revisa la entrada de texto
    @objc fileprivate func CntrlaTextoInput() {
        
        //Captura los datos de entrada
        let ValidDatos = nombres.text?.count ?? 0 > 0 && apellidos.text?.count ?? 0 > 0 && username.text?.count ?? 0 > 0 && email.text?.count ?? 0 > 0 && password.text?.count ?? 0 > 0
        
        
        //Verifica si contiene datos validos para habilitar el boton de crear cuenta
        if ValidDatos {
            RegistarBtn.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.4)
            //RegistarBtn.isEnabled = false
        } else {
            RegistarBtn.isEnabled = true
            RegistarBtn.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.7)
        }
    }

    //Manda a crear la cuenta
    @objc fileprivate func CrearCuenta() {
        
        //Captura los datos
        guard let nombres = nombres.text, nombres.count > 0 else { return }
        guard let apellidos = apellidos.text, apellidos.count > 0 else { return }
        guard let username = username.text, username.count > 0 else { return }
        guard let correo = email.text, correo.count > 0 else { return }
        guard let password = password.text, password.count > 0 else { return }
        
        //Metodo firebase para crear cuenta
        Auth.auth().createUser(withEmail: correo, password: password) { (User, Error) in
            //Detecta si ocurrio algun error
            if let err = Error {
                
                print("Ocurrio un error al crear la cuenta:\(err)")
                
                DispatchQueue.main.async {
                    //Contiene el mensaje de error
                    let mg = UILabel()
                    mg.textColor = UIColor.white
                    mg.textAlignment = .center
                    mg.font = UIFont(name: "Avenir Next", size: 17)
                    mg.text = "Oops something went wrong!"
                    mg.numberOfLines = 0
                    mg.layer.zPosition = 1
                    mg.clipsToBounds = true
                    mg.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.6)
                    mg.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 8)
                    
                    //Se agrega a la vista
                    self.view.addSubview(mg)
                    
                    //Proceso de animacion
                    UIView.animate(withDuration: 1.5, delay: 3, options: .curveEaseOut, animations: {
                        mg.alpha = 0.0
                    }, completion: { (_) in
                        mg.removeFromSuperview()
                    })
                }
            }
            
            //Foto de perfil
            guard let imgPerfil = self.Avataruser.imageView?.image else {
                return
            }
            
            //Dato a subir
            guard let SubirDato = imgPerfil.jpegData(compressionQuality: 0.3) else {
                return
            }
            
            //Nombre del archivo con un ID unico
            let nombreArchivo = UUID().uuidString
            
            //Manda a subir la foto de perfil del usuario
            Storage.storage().reference().child("FotoPerfil").child(nombreArchivo).putData(SubirDato, metadata: nil, completion: { (metadata, error) in
                
                //Detecta si ocurrio algun error
                if let err = error {
                    print("Oops algo ocurrio al subir la foto de perfil:\(err)")
                    return
                }
                
                //Obtiene la url del archivo
                Storage.storage().reference().child("FotoPerfil/\(nombreArchivo)").downloadURL(completion: { (url, error) in
                    
                    //Detecta si ocurrio algun error
                    if let err = error {
                        
                        print("Oops algo va mal:\(err)")
                    }
                    
                    //Almacena la url del archivo
                    guard let FotoPerfilURL = url?.absoluteString else {
                        return
                    }
                    
                    print("Esta es la URL de la foto de perfil del usuario:\(FotoPerfilURL)")
                    
                    //Captura el id del usuario
                    guard let uid = User?.user.uid else {
                        return
                    }
                    
                    //Captura los valores a guardar en la base de datos
                    let valoresGuardar = ["FotoPerfilURL":FotoPerfilURL,"nombres":nombres,"apellidos":apellidos,"username":username,"email":correo,"password":password]
                    
                    //Asigana el id del usuario a su respectiva informacion
                    let valores = [uid:valoresGuardar]
                    
                    if valores.isEmpty {
                        print("Oops los valores estan vacios")
                        
                    } else {
                        
                        //Metodo firebase para crear y registrar datos en la base de datos
                        Database.database().reference().child("users").updateChildValues(valores, withCompletionBlock: { (error, ref) in
                            //Detecta si ocurrio algun error
                            if let err = error {
                                print("Oops algo va mal:\(err)")
                            } else {
                                
                                print("Tabla de users creada")
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                //Carga el controlador raiz
                                guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController  else { return }
                                
                                //Accede a la funcion de controladores
                                mainTabBarController.PreparaVControllers()
                            }
                        })
                    }
                    
                    //Resultado satisfactorio
                    print("Se logro surbir la foto de perfil")
                })
            })
        }
    }
    
    //Carga screen login
    @objc func SignIn() {
        let log = LoginController()
        navigationController?.pushViewController(log, animated: true)
    }
    
    //Manda a esconder el teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Habilita el boton de return para seguir con el siguiente TextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = apellidos.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
}
