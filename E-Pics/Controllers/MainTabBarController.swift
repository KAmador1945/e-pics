//
//  MainTabBarController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    
        //Manda a llamar las funciones
        PreparaVControllers()
    }
    
    //Manda a seleccionar por defecto el controlador
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        //Indexa las vistas
        let index = tabBarController.viewControllers?.firstIndex(of: viewController)
        
        //Detecta si el tab fue seleccionado para mostrar el carrete
        if index == 2 {
            //Inicializa el constructor
            let layout = UICollectionViewFlowLayout()
            
            //Inicializa el collectionview
            let selectPicController = SelectPicController(collectionViewLayout: layout)
            let navController = UINavigationController(rootViewController: selectPicController)
            
            //Carga el controlador
            present(navController, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    //Prepara ViewControllers
    func PreparaVControllers() {
        
        //Prepara si el usuario esta conectado
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                
                //Home Controller
                let homeNavController = self.TemplateControllers(unselected: #imageLiteral(resourceName: "home_unselected.png"), selected: #imageLiteral(resourceName: "home_selected.png"), rootViewController: HomeController(collectionViewLayout: UICollectionViewFlowLayout()))
                
                //Search Controller
                let searchNavController = self.TemplateControllers(unselected: #imageLiteral(resourceName: "search_unselected.png"), selected: #imageLiteral(resourceName: "search_selected.png"), rootViewController: UserSearchController(collectionViewLayout: UICollectionViewFlowLayout()))
                
                //Upload Controller
                let uploadNavController = self.TemplateControllers(unselected:#imageLiteral(resourceName: "pic_unselected"), selected: #imageLiteral(resourceName: "pic_selected"))
                
                //Notify Controller
                let notifyNavController = self.TemplateControllers(unselected: #imageLiteral(resourceName: "notify_unselected.png"), selected: #imageLiteral(resourceName: "notify_selected.png"))
                
                //Carga el controlador y se inicializa el collectionview
                let layout = UICollectionViewFlowLayout()
                let userProfileController = UserProfileController(collectionViewLayout: layout)
                
                //User Profile Controller
                let UserProfilenavController = UINavigationController(rootViewController: userProfileController)
                
                //Carga icono sin seleccionar
                UserProfilenavController.tabBarItem.image = #imageLiteral(resourceName: "profile.png")
                
                //Carga icono seleccionado
                UserProfilenavController.tabBarItem.selectedImage = #imageLiteral(resourceName: "profile_selected.png")
                
                //Color que usara el tabbar
                self.tabBar.tintColor = .black
                
                
                //Almacena los controladores en un arreglo
                self.viewControllers = [homeNavController,
                                        searchNavController,
                                        uploadNavController,
                                        notifyNavController,
                                        UserProfilenavController]
                
                //Recorre los items del tabbar para ubicarlos de manera correcta
                guard let items = self.tabBar.items else { return }
                
                for item in items {
                    item.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
                }
                
            } else {
                //Carga en segundo plano
                DispatchQueue.main.async {
                    let loginController = LoginController()
                    let UserProfilenavController = UINavigationController(rootViewController: loginController)
                    self.present(UserProfilenavController, animated: true, completion: nil)
                }
                
                return
            }
        }
    }
    
    //Template de los controladores
    fileprivate func TemplateControllers(unselected: UIImage, selected: UIImage, rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        
        //Controller
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselected
        navController.tabBarItem.selectedImage = selected
        
        //Retorna el objeto
        return navController
    }
}
