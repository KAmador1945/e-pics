//
//  UserProfileHeader.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

protocol UserProfileHeaderDelegate {
    func didChangeToList()
    func didChanceToGrid()
}

class UserProfileHeader: UICollectionViewCell {
    
    //Asigna el valor
    var delegate: UserProfileHeaderDelegate?
    
    //Carga Avatar
    lazy var Avatar: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        let img = CustumImagenView(image: ava)
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 7
        img.layer.borderColor = UIColor.white.cgColor
        img.layer.borderWidth = 2
        img.layer.zPosition = 1
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Blur filter
    let BlurEffecto: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        var blur = CustumImagenView(image: ava)
        blur.contentMode = .scaleAspectFill
        blur.clipsToBounds = true
        blur.translatesAutoresizingMaskIntoConstraints = false
        return blur
    }()

    //Nanme of user
    let NameuserLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "Real Name User"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 20)
        lbl.textColor = UIColor.white
        lbl.layer.zPosition = 1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //Username
    let UsernameLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "Username"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 18)
        lbl.textColor = UIColor.white
        lbl.layer.zPosition = 1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //Boton editar perfil
    lazy var EditarPrfil: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("•••", for: .normal)
        btn.tintColor = UIColor.black
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 17)
        btn.clipsToBounds = true
        btn.layer.zPosition = 1
        btn.isHidden = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Boton seguir usuario
    lazy var FollowPrfil: UIButton = {
        let btn = UIButton(type: .system)
        btn.tintColor = UIColor.black
        btn.layer.cornerRadius = 5
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 13)
        btn.clipsToBounds = true
        btn.layer.zPosition = 1
        btn.isHidden = true
        btn.layer.borderColor = UIColor.lightGray.cgColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(FollowUsr), for: .touchUpInside)
        return btn
    }()
    
    let LikesLbl: UILabel = {
        let lbl = UILabel()
        let atributoText = NSMutableAttributedString(string: "0", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.black])
        atributoText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 4)]))
        atributoText.append(NSAttributedString(string: " Likes", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.gray]))
        lbl.attributedText = atributoText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let FollowingLbl: UILabel = {
        let lbl = UILabel()
        let atributoText = NSMutableAttributedString(string: "0", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.black])
        atributoText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 4)]))
        atributoText.append(NSAttributedString(string: " Followings", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.gray]))
        lbl.attributedText = atributoText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let FollowersLbl: UILabel = {
        let lbl = UILabel()
        let atributoText = NSMutableAttributedString(string: "0", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.black])
        atributoText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 4)]))
        atributoText.append(NSAttributedString(string: " Followers", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.gray]))
        lbl.attributedText = atributoText
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    
    //boton de grid
    lazy var GridBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "grid.png")
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.purple
        btn.addTarget(self, action: #selector(ManejaGrid), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //boton de list
    lazy var ListBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "list.png")
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor(white: 0, alpha: 0.1)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(ManejaLista), for: .touchUpInside)
        return btn
    }()
    
    //Collection Pics
    let CollectionPicLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "Collection Pics"
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight(rawValue: 600))
        lbl.layer.zPosition = 1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //Inicializa las celdas del collectionview
    override init(frame: CGRect) {
        
        //Inicializa la celda
        super.init(frame: frame)
        
        //Define el color de fondo
        backgroundColor = .white
        
        //Manda a llamar las funciones
        UbicaAvatar()
        UbicaDatosUsr()
        UbicaStats()
        UbicaToolbar()
        ExecuteLoadController()
    }
    
    //Acede al diccionario que contiene la info del usuario
    var user: User? {
        
        didSet {
            
            DispatchQueue.main.async {
                
                //Caprura la url de la foto de perfil
                guard let AvatarProfileURL = self.user?.fotoPerfilURL else { return }
                
                //Manda a descargar la foto de perfil
                self.Avatar.CargaImagen(urlString: AvatarProfileURL)
                self.BlurEffecto.CargaImagen(urlString: AvatarProfileURL)
                
                //Pasa el dato al label
                self.NameuserLbl.text = self.user?.nombreCompleto
                self.UsernameLbl.text = self.user?.username
            }
            
            //Manda a llamar las funciones
            CheckFollowsUsr()
            //CountLike()
        }
    }
    
    //Manda a ubicar la foto de perfil
    fileprivate func UbicaAvatar() {
        
        //Se agrega a la vista
        addSubview(Avatar)
        addSubview(BlurEffecto)
        addSubview(CollectionPicLbl)
        
        //Manda a ubicar
        NSLayoutConstraint.activate([
            Avatar.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            Avatar.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            Avatar.widthAnchor.constraint(equalToConstant: 100),
            Avatar.heightAnchor.constraint(equalToConstant: 150)
        ])
        
        NSLayoutConstraint.activate([
            BlurEffecto.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            BlurEffecto.leftAnchor.constraint(equalTo: leftAnchor),
            BlurEffecto.rightAnchor.constraint(equalTo: rightAnchor),
            BlurEffecto.heightAnchor.constraint(equalToConstant: 120)
        ])
        
        NSLayoutConstraint.activate([
            CollectionPicLbl.topAnchor.constraint(equalTo: Avatar.bottomAnchor, constant: 15),
            CollectionPicLbl.leftAnchor.constraint(equalTo: Avatar.leftAnchor, constant: 0)
        ])
        
        //Se manda a aplicar el efecto de blur
        let darkMode = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: darkMode)
        blurView.frame = BlurEffecto.bounds
        //blurView.alpha = 0.9
        blurView.frame = CGRect(x: 0, y: 0, width: self.BlurEffecto.frame.width * frame.width, height: 120)
        
        //Agregando a la vista
        addSubview(blurView)
    }
    
    //Manda a ubicar los datos del usuario
    fileprivate func UbicaDatosUsr() {
        
        //Se agregan los objetos a la vista
        
        addSubview(NameuserLbl)
        addSubview(UsernameLbl)
        addSubview(EditarPrfil)
        
        //Constraint
        NSLayoutConstraint.activate([
            NameuserLbl.leftAnchor.constraint(equalTo: Avatar.rightAnchor, constant: 10),
            NameuserLbl.topAnchor.constraint(equalTo: Avatar.topAnchor, constant: 5)
        ])
        
        NSLayoutConstraint.activate([
            UsernameLbl.leftAnchor.constraint(equalTo: Avatar.rightAnchor, constant: 10),
            UsernameLbl.topAnchor.constraint(equalTo: NameuserLbl.bottomAnchor, constant: 5)
        ])

        NSLayoutConstraint.activate([
            EditarPrfil.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            EditarPrfil.rightAnchor.constraint(equalTo: rightAnchor, constant: -15)
        ])
    }
    
    //Manda a ubicar las estadisticas del usuario
    fileprivate func UbicaStats() {
        
        //Se crea stackview
        let stackView = UIStackView(arrangedSubviews: [LikesLbl,FollowingLbl,FollowersLbl,FollowPrfil])
        
        //Configuracion del stackview
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        stackView.layer.borderColor = UIColor.black.cgColor
        stackView.layer.borderWidth = 2
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stackView)

        //Constraint
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: BlurEffecto.bottomAnchor, constant: 10),
            stackView.leftAnchor.constraint(equalTo: BlurEffecto.leftAnchor, constant: 100),
            stackView.rightAnchor.constraint(equalTo: BlurEffecto.rightAnchor, constant: -5),
            stackView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    //Manda a ubicar el toolbar
    fileprivate func UbicaToolbar() {
        
        //Linea separadora
        let topSeparator = UIView()
        topSeparator.backgroundColor = UIColor.lightGray
        topSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomSeparator = UIView()
        bottomSeparator.backgroundColor = UIColor.lightGray
        bottomSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        //Stackview
        let stackview = UIStackView(arrangedSubviews: [GridBtn,ListBtn])
        
        //Se define la distribucion y el eje
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        stackview.translatesAutoresizingMaskIntoConstraints = false
        
        //Se manda a agregar a la vista
        addSubview(stackview)
        addSubview(topSeparator)
        addSubview(bottomSeparator)
        
        //Se manda a unicar el stackview
        NSLayoutConstraint.activate([
            stackview.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            stackview.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            stackview.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            stackview.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        
        //Linea separadora de arriba
        NSLayoutConstraint.activate([
            topSeparator.topAnchor.constraint(equalTo: stackview.topAnchor),
            topSeparator.leftAnchor.constraint(equalTo: leftAnchor),
            topSeparator.rightAnchor.constraint(equalTo: rightAnchor),
            topSeparator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        //Linea separadora de abajo
        NSLayoutConstraint.activate([
            bottomSeparator.bottomAnchor.constraint(equalTo: stackview.bottomAnchor),
            bottomSeparator.leftAnchor.constraint(equalTo: stackview.leftAnchor),
            bottomSeparator.rightAnchor.constraint(equalTo: stackview.rightAnchor),
            bottomSeparator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
   
    //Funcion de seguir a usuario
    @objc fileprivate func FollowUsr() {
        
        //Captura el id del usuario en sesion
        guard let currentUserID = Auth.auth().currentUser?.uid else { return }
        
        //Id del usuario a seguir
        guard let userID = user?.uid else { return }
        
        //Checa si esta siguiendo o no para cambiar el titulo del boton
        if FollowPrfil.titleLabel?.text == "Unfollow" {
            
            Database.database().reference().child("following").child(currentUserID).child(userID).removeValue { (error, ref) in
                
                //Detecta si ocurrio algun error
                if let err = error {
                    print("Oops algo va mal, no se logro dejar de seguir a este usuario", err)
                } else {
                    //Mensaje
                    print("Has dejado de seguir a este usuario", self.user?.username ?? "")
                    self.FollowPrfil.setTitle("Follow", for: .normal)
                    self.FollowPrfil.isHidden = false
                }
            }
            
        } else {
            
            //Valores a insertar
            let values = [userID: 1]
            
            //Se manda a crear la referencia
            let ref = Database.database().reference().child("following").child(currentUserID)
            
            //Metodo para realizar la insercion en la nueva tabla de following
            ref.updateChildValues(values) { (err, ref) in
                
                //Detecta si ocurrio algun error
                if let err = err {
                    print("Oops algo ocurrion, no se puede seguir al usuario", err)
                } else {
                    //Mensaje
                    print("Grandioso ya estas siguiendo a esta persona", self.user?.username ?? "")
                    self.FollowPrfil.setTitle("Unfollow", for: .normal)
                    self.FollowPrfil.isHidden = false
                }
            }
        }
    }
    
    
    //Checa si el usuario actual en sesion esta siguiendo a otro
    fileprivate func CheckFollowsUsr() {
        
        //current user
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        
        //Captura el id del usuario
        guard let FollowUsrId = user?.uid else { return }
        
        //Detecta si el id del usuario es igual al usuario en sesion
        if FollowUsrId != currentUser {
            
            //Detecta si el usuario actual en sesion esta siguiendo a otro
            Database.database().reference().child("following").child(currentUser).child(FollowUsrId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                //Detecta si se esta siguiendo al usuario cuando el valor obtenido es 1
                if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
                    
                    //Cambia el texto del boton de seguir a dejar de seguir
                    self.FollowPrfil.setTitle("Unfollow", for: .normal)
                    self.FollowPrfil.isHidden = false
                    self.EditarPrfil.isHidden = true
                } else {
                    self.FollowPrfil.setTitle("Follow", for: .normal)
                    self.FollowPrfil.isHidden = false
                }
                
            }, withCancel: { (error) in
                print("Oops algo ocurrio al dejar de seguir al usuario")
            })
        } else {
            FollowPrfil.isHidden = true
            EditarPrfil.isHidden = false
        }
    }
    
    //Count all like from post user
    fileprivate func CountLike() {
        
        //Ref to database
        let ref = Database.database().reference().child("likes")
        
        //Data of user
        guard let user = self.user else { return }
        
        //Get id geust user and current user
        guard let current_user = Auth.auth().currentUser?.uid else { return }
        guard let guest_user = self.user?.uid else { return }
        
        //Check both id
        if guest_user != current_user {
            print("Los id son diferentes")
            
            //Fire method
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                //Diccionary
                guard let diccionary = snapshot.value as? [String:Any] else { return }
                
                //Go every data from diccionary
                diccionary.forEach({ (data) in
                    
                    //Get data from diccionary
                    let (key,_) = data
                    
                    //Access to struct post for to get id post
                    var post = PostUsr(user: user, diccionarioPost: diccionary)
                    
                    //Access to id post
                    post.id = key
                    
                    print("Llave del post:\(key)")
                    
                    //user on session
                    
                    let ref_likes = Database.database().reference().child("likes").child(key).child(current_user)
                    
                    //Fire method
                    ref_likes.observe(.value, with: { (snapshot) in
                        //Count data
                        guard let values = snapshot.value as? Int else { return }
                        
                        print("valores:\(values)")
                        let complte = values + 1
                        DispatchQueue.main.sync {
                            self.LikesLbl.text = String(complte)
                        }
                    })
                    
                })
            }, withCancel: { (Error) in
                print("Oops something god mad:\(Error)")
            })
        } else {
            print("Los id son iguales")
        }
    }
    
    //Controla la forma de ver collection pics a grid
    @objc fileprivate func ManejaGrid() {
        print("Cambiando a grid")
        delegate?.didChanceToGrid()
        GridBtn.tintColor = UIColor.mainPurple()
        ListBtn.tintColor = UIColor.mainGray()
    }
    
    //Controla la forma de ver collection pics a list
    @objc fileprivate func ManejaLista() {
        print("Cambiando a lista")
        delegate?.didChangeToList()
        GridBtn.tintColor = UIColor.mainGray()
        ListBtn.tintColor = UIColor.mainPurple()
    }
    
    func ExecuteLoadController() {
        EditarPrfil.addTarget(self, action: #selector(LoadEditProfileController), for: .touchUpInside)
    }
    
    @objc func LoadEditProfileController() {
        let controller = EditProfileController()
       UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
