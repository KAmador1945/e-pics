//
//  UserProfileController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class UserProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UserProfileHeaderDelegate {
    
    //Inicia seleccionado por defecto
    var isGridView = true
    
    //Identificador de la celda
    let celda_id = "CeldaID"
    let home_celda = "HomeID"
    
    //Almacena el id del usuario
    var userId: String?
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Connect with controller
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateProfile), name: EditProfileController.nameNotify, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateProfileFromeHome), name: HomeController.updateProfile, object: nil)
        
        //Define background color
        collectionView.backgroundColor = .white
        
        //Se registra la clase que crea al header
        collectionView.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header_ID")
        
        //Se registra la clase de las celdas del perfil y home
        collectionView.register(UserProfilePicCell.self, forCellWithReuseIdentifier: celda_id)
        collectionView.register(HomePostCell.self, forCellWithReuseIdentifier: home_celda)
        
        //Manda a llamar las funciones
        DescargaInfoUsr()
        PaginationPost()
        PreparaLogOut()
    }
    

    //Refresh profile
    @objc func UpdateProfile() {
        DispatchQueue.main.async {
            self.DescargaInfoUsr()
            self.postUsr.removeAll()
        }
    }
    
    @objc func UpdateProfileFromeHome() {
        DispatchQueue.main.async {
            self.PaginationPost()
            self.postUsr.removeAll()
        }
    }
    
    //Se manda a crear un header
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        //Se manda a crear el header
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header_ID", for: indexPath) as! UserProfileHeader
        
        //Se accede a la variable que maneja datos globalmente en el header
        header.user = self.users
        header.delegate = self
        
        return header
    }
    
    //Define la medida de las celdas del perfil
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Detecta en que tipo de vista es seleccionada
        if isGridView {
            let width = (view.frame.width - 2) / 2
            return CGSize(width: width, height: width)
        } else {
            var height: CGFloat = 40 + 8 + 8
            height += view.frame.width
            height += 60
            height += 60
            
            return CGSize(width: view.frame.width, height: height)
        }
    }
    
    //Define el numero de celdas
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //Muestra los datos contenidos en el arreglo
        return postUsr.count
    }
    
    //Define el minimo de espacio entre las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    //Define espacio entre celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    //Define la celda por cada items
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Show call for pagination is finished
        if indexPath.item == self.postUsr.count - 1  && !isFinishedPage {
            print("pagination post")
            DispatchQueue.main.async {
                self.PaginationPost()
            }
        }
        
        //Detecta si la vista esta en grid
        if isGridView {
            //Se manda a reutilizar las celdas
            let celda = collectionView.dequeueReusableCell(withReuseIdentifier: celda_id, for: indexPath) as! UserProfilePicCell
            //Color de fondo
            celda.postData = postUsr[indexPath.item]
            return celda
        } else {
            let celda = collectionView.dequeueReusableCell(withReuseIdentifier: home_celda, for: indexPath) as! HomePostCell
            celda.posts = postUsr[indexPath.item]
            return celda
        }
    }
    
    //Se manda a asignar la medida del header
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 270)
    }
    
    //Maneja los datos a nivel global
    var users: User?
    
    //Manda a descargar la informacion del usuario
    fileprivate func DescargaInfoUsr() {
        
        //Carga el id del usuario
        let uid = userId ?? (Auth.auth().currentUser?.uid ?? "")
        
        //ejecuta el bloque de manera asincrono
        DispatchQueue.main.async {
            
            //Accede al metodo de descarga de info del usuario
            Database.DescargaUsrConUID(uid: uid) { (user) in
                
                //Accede a la info
                self.users = user
                
                //Concatena datos
                let datos_completos = (self.users?.nombre)! + " " + (self.users?.apellido)!
                
                //Escribe el username en la barra de navegacion
                self.navigationItem.title = datos_completos
                
                //Refresca collectionview en segundo plano
                self.collectionView.reloadData()
                
                //Descarga los posts del usuario
                //self.DescargaPostOrdered()
                self.PaginationPost()
            }
        }
        
        collectionView.reloadData()
    }
    
    //Variable global de post
    var isFinishedPage = false
    var postUsr = [PostUsr]()
    
    //Pagination Post User
    fileprivate func PaginationPost() {
        
        //Capture id user
        guard let uid = self.users?.uid else { return }
        
        //Firebase ref
        let ref = Database.database().reference().child("posts").child(uid)
        
        var query = ref.queryOrdered(byChild: "date")

        //Check array is major than 0
        if postUsr.count > 0 {
            let value = postUsr.last?.postDate.timeIntervalSince1970
            query = query.queryEnding(atValue: value)
        }
        
        query.observeSingleEvent(of: .value) { (snapshot) in
            //Cast all objects
            guard var allObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            
            //Staring array reverse way
            allObjects.reverse()
            
            //Check if pagination is finished
//            DispatchQueue.main.async {
//                if allObjects.count < 1 {
//                    self.isFinishedPage = true
//                }
//            }
            
            //Remove the last object for do not repeat data
            if self.postUsr.count > 0  && allObjects.count > 0 {
                allObjects.removeFirst()
            }
            
            //Capture info user
            guard let user = self.users else { return }
            
            //go every objects with own key
            allObjects.forEach({ (snapshot) in
                
                //Create a dictionary
                guard let dictionary = snapshot.value as? [String:Any] else { return }
                
                //Load all post on this variable
                var post = PostUsr(user: user, diccionarioPost: dictionary)
                
                post.id = snapshot.key
                
                //Insert data on post user array
                self.postUsr.append(post)
            })
            
            //reload collectionView
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
//        //Firebase method for download data from database
//        query.queryLimited(toLast: 1).observeSingleEvent(of: .value) { (snapshot) in
//
//            //Cast all objects
//            guard var allObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
//
//            //Staring array reverse way
//            allObjects.reverse()
//
//            //Check if pagination is finished
//            DispatchQueue.main.async {
//                if allObjects.count < 1 {
//                    self.isFinishedPage = true
//                }
//            }
//
//            //Remove the last object for do not repeat data
//            if self.postUsr.count > 0  && allObjects.count > 0 {
//                allObjects.removeFirst()
//            }
//
//            //Capture info user
//            guard let user = self.users else { return }
//
//            //go every objects with own key
//            allObjects.forEach({ (snapshot) in
//
//                //Create a dictionary
//                guard let dictionary = snapshot.value as? [String:Any] else { return }
//
//                //Load all post on this variable
//                var post = PostUsr(user: user, diccionarioPost: dictionary)
//
//                post.id = snapshot.key
//
//                //Insert data on post user array
//                self.postUsr.append(post)
//            })
//
//            //reload collectionView
//            DispatchQueue.main.async {
//                self.collectionView.reloadData()
//            }
//        }
    }
    
    //Manda a descargar los posts de manera ordenados realizados por el usuario
    fileprivate func DescargaPostOrdered() {
        
        //Captura el id del usuario en sesion
        guard let uid = self.users?.uid else { return }
        
        //Se manda a crear la referencia
        let ref = Database.database().reference().child("posts").child(uid).queryOrdered(byChild: "date")
        
        DispatchQueue.main.async {
            
            //Manda a descargar los datos
            ref.observe(.childAdded, with: { (snapshot) in
                
                //Se manda a crear el diccionario
                guard let diccionario = snapshot.value as? [String: Any] else { return }
                
                //Accede el username del usuario
                guard let user = self.users else { return }
                
                //Se almacena el diccionario en el arreglo
                let posts = PostUsr(user: user, diccionarioPost: diccionario)
                
                //Ordena el arreglo de forma inversa
                self.postUsr.insert(posts, at: 0)
                
                //Ordena los posts segun su fecha
                self.postUsr.sort(by: { (p1, p2) -> Bool in
                    return p1.postDate.compare(p2.postDate) == .orderedDescending
                })
        
                //Refresca los datos
                self.collectionView.reloadData()
                
            }, withCancel: { (error) in
                print("Oops error al descargar los posts", error)
            })
        }
    }
    
    //Manda a cerrar la sesion
    fileprivate func PreparaLogOut() {
        //Agrega boton de cerrar sesion
        let img = UIImage(named: "logout.png")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: img?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(CerrarSesion))
    }
    
    //Manda a cerrar la session
    @objc fileprivate func CerrarSesion() {
        
        //Almacena la alerta
        let alertController = UIAlertController(title: "", message: "Are you sure?", preferredStyle: .actionSheet)
        
        //Se agrega la accion al controlador
        alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
            
            //Manda a cerrar la sesion
            do {
                
                //Manda a cerrar la session.
                try Auth.auth().signOut()
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                
                //Carga el contralador
                self.present(navController, animated: true, completion: nil)
                
            } catch let Error {
                print("Ocurrion un error al cerrar la sesion", Error)
            }
        }))
        
        //Se agrega el boton de cancelar
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //Manda a presentar el controlador
        present(alertController, animated: true, completion: nil)
    }
    
    //Cambia la vista a lista
    func didChangeToList() {
        isGridView = false
        collectionView.reloadData()
    }
    
    //Cambia la vista a grid
    func didChanceToGrid() {
        isGridView = true
        collectionView.reloadData()
    }
}
