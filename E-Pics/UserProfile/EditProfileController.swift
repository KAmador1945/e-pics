//
//  EditProfileController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 4/1/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class EditProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Define el tipp de dato del id del usuario
    var userId: String?
    
    //NavBar
    let navBar: UINavigationBar = {
        let nb = UINavigationBar()
        nb.barTintColor = UIColor(red: 255, green: 255, blue: 255, alpha: 255)
        nb.translatesAutoresizingMaskIntoConstraints = false
        return nb
    }()
    
    //Button rightnavbar
    let SavBtn: UIBarButtonItem = {
        let btn = UIBarButtonItem()
        btn.style = .plain
        btn.title = "Save"
        btn.action =  #selector(SaveProfile)
        return btn
    }()
    
    //Carga Avatar
    lazy var Avatar: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        let img = CustumImagenView(image: ava)
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 7
        img.layer.borderColor = UIColor.white.cgColor
        img.layer.borderWidth = 2
        img.layer.zPosition = 1
        img.clipsToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Blur filter
    let BlurEffecto: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        var blur = CustumImagenView(image: ava)
        blur.contentMode = .scaleAspectFill
        blur.clipsToBounds = true
        blur.translatesAutoresizingMaskIntoConstraints = false
        return blur
    }()
    
    //Edit pic profile btn
    let EditProfBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "edit.png")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.layer.zPosition = 1
        btn.addTarget(self, action: #selector(UpdateProfilePic), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Real name
    let FirstnameLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Firstname:"
        lbl.textColor = .purple
        lbl.font = UIFont(name: "Avenir Next", size: 18)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let LastNameLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Lastname:"
        lbl.textColor = .purple
        lbl.font = UIFont(name: "Avenir Next", size: 18)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //Username
    let UsernameLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Username:"
        lbl.textColor = .purple
        lbl.font = UIFont(name: "Avenir Next", size: 18)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    //Textfield
    let FirstnameTxt: UITextField = {
        let tv = UITextField()
        tv.textAlignment = .left
        tv.borderStyle = .roundedRect
        tv.textColor = UIColor.purple
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let LastnameTxt: UITextField = {
        let tv = UITextField()
        tv.textAlignment = .left
        tv.borderStyle = .roundedRect
        tv.textColor = UIColor.purple
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let UsernameTxt: UITextField = {
        let tv = UITextField()
        tv.textAlignment = .left
        tv.borderStyle = .roundedRect
        tv.textColor = UIColor.purple
        tv.autocapitalizationType = .none
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define el color de fondo
        view.backgroundColor = UIColor.white
        
        //Manda a llamar las funciones
        SetNavigationBar()
        UbicaAvatar()
        UbicaObjsUsrs()
        UsrData()
    }
    
    //Create navigation bar
    func SetNavigationBar() {

        let navItem = UINavigationItem(title: "Edit Profile")
        navItem.rightBarButtonItem = SavBtn
        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(CancelEdit))
        navItem.leftBarButtonItem = cancelBtn
        navBar.setItems([navItem], animated: true)
        navigationController?.navigationBar.tintColor = UIColor.purple
        self.view.addSubview(navBar)
        
        //Checa si esta disponible
        if #available(iOS 11, *) {
            let guia = view.safeAreaLayoutGuide
            navBar.topAnchor.constraint(equalTo: guia.topAnchor).isActive = true
        } else {
            navBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        }
        
        //Constraint complements
        navBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        navBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        navBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    }
    
    
    //Manda a ubicar la foto de perfil
    fileprivate func UbicaAvatar() {
        
        //Se agrega a la vista
        view.addSubview(Avatar)
        view.addSubview(BlurEffecto)
        view.addSubview(EditProfBtn)
        
        //Manda a ubicar
        NSLayoutConstraint.activate([
            Avatar.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            Avatar.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 70),
            Avatar.widthAnchor.constraint(equalToConstant: 100),
            Avatar.heightAnchor.constraint(equalToConstant: 150)
        ])
        
        NSLayoutConstraint.activate([
            BlurEffecto.leadingAnchor.constraint(equalTo: navBar.leadingAnchor),
            BlurEffecto.trailingAnchor.constraint(equalTo: navBar.trailingAnchor),
            BlurEffecto.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 44),
            BlurEffecto.heightAnchor.constraint(equalToConstant: 120)
        ])
        
        NSLayoutConstraint.activate([
            EditProfBtn.leftAnchor.constraint(equalTo: Avatar.leftAnchor, constant: 7),
            EditProfBtn.bottomAnchor.constraint(equalTo: Avatar.bottomAnchor, constant: -7),
            EditProfBtn.widthAnchor.constraint(equalToConstant: 20),
            EditProfBtn.heightAnchor.constraint(equalToConstant: 20)
        ])

        //Se manda a aplicar el efecto de blur
        let darkMode = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: darkMode)
        blurView.frame = BlurEffecto.bounds
        blurView.translatesAutoresizingMaskIntoConstraints = false
        //blurView.alpha = 0.9
        
        
        //Agregando a la vista
        view.addSubview(blurView)
        blurView.leadingAnchor.constraint(equalTo: navBar.leadingAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: navBar.trailingAnchor).isActive = true
        blurView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 44).isActive = true
        blurView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
    }
    
    //Agrega etiquetas del usuario
    fileprivate func UbicaObjsUsrs() {
        
        //Inicializa el stackview
        let stackView = UIStackView(arrangedSubviews: [FirstnameLbl, LastNameLbl, UsernameLbl])
        
        //Se configura
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 30
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        //Se agrega la vista
        view.addSubview(stackView)
        
        //Constraint
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -view.frame.width / 3),
            stackView.topAnchor.constraint(equalTo: Avatar.bottomAnchor, constant: 30),
        ])
        
        //Agrega a la vista
        view.addSubview(FirstnameTxt)
        view.addSubview(LastnameTxt)
        view.addSubview(UsernameTxt)
        
        //Constraint data
        NSLayoutConstraint.activate([
            FirstnameTxt.leftAnchor.constraint(equalTo: FirstnameLbl.rightAnchor, constant: 10),
            FirstnameTxt.topAnchor.constraint(equalTo: FirstnameLbl.topAnchor, constant: 0),
            FirstnameTxt.widthAnchor.constraint(equalToConstant: 200 - FirstnameLbl.frame.width),
        ])
        
        NSLayoutConstraint.activate([
            LastnameTxt.leftAnchor.constraint(equalTo: LastNameLbl.rightAnchor, constant: 10),
            LastnameTxt.topAnchor.constraint(equalTo: LastNameLbl.topAnchor, constant: 0),
            LastnameTxt.widthAnchor.constraint(equalToConstant: 200 - LastNameLbl.frame.width),
        ])
        
        NSLayoutConstraint.activate([
            UsernameTxt.leftAnchor.constraint(equalTo: UsernameLbl.rightAnchor, constant: 10),
            UsernameTxt.topAnchor.constraint(equalTo: UsernameLbl.topAnchor, constant: 0),
            UsernameTxt.widthAnchor.constraint(equalToConstant: 200 - UsernameLbl.frame.width),
        ])
    }
    
    //Captura usuario en sesion
    var users: User?
    fileprivate func UsrData() {
        
        //Captura el id del usuario en sesion
        let uid = userId ?? (Auth.auth().currentUser?.uid ?? "")
        
        //Descarga la info del usuario segun su id
        Database.DescargaUsrConUID(uid: uid) { (user) in
            self.users = user
            
            //Firstname
            guard let firstname = self.users?.nombre else { return }
            self.FirstnameTxt.text = firstname
            
            //Lastname
            guard let lastname = self.users?.apellido else { return }
            self.LastnameTxt.text = lastname
            
            //Username
            guard let username = self.users?.username else { return }
            self.UsernameTxt.text = username
            
            //Avatar y blur efecto
            guard let avatar = self.users?.fotoPerfilURL else { return }
            self.Avatar.CargaImagen(urlString: avatar)
            self.BlurEffecto.CargaImagen(urlString: avatar)
        }
    }
    
    //Carga carrete de fotos
    @objc fileprivate func UpdateProfilePic() {
        print("Seleccionando foto")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    //Obtiene informacion sobre la foto, si fue editada o no
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //Detecta si se realizo cambio a la foto
        if let FotoEditada = info[.editedImage] as? UIImage {
            
            //Se inserta la foto en su contenedor
            Avatar.image = FotoEditada
        }
        
        //Oculta el controlador
        dismiss(animated: true, completion: nil)
    }
    
    //Valor estatico para refrescar los datos
    static let nameNotify = NSNotification.Name("UpdateProfile")
    
    //Guarda las nuevas configuraciones
    @objc fileprivate func SaveProfile() {
        
        //Deshabilita el boton de guardar
        self.SavBtn.isEnabled = false
        
        //Datos a guardar
        guard let first_name = FirstnameTxt.text else { return }
        guard let last_name = LastnameTxt.text else { return }
        guard let username = UsernameTxt.text else { return }
        
        //Foto de perfil
        guard let imgPerfil = self.Avatar.image else { return }
       
        //Comprime dato
        guard let subirDato = imgPerfil.jpegData(compressionQuality: 0.3) else { return }
        
        //Nombre del archivo
        let nombreArvhivo = UUID().uuidString
        
        //Manda a subir la foto de perfil del usuario
        Storage.storage().reference().child("FotoPerfil").child(nombreArvhivo).putData(subirDato, metadata: nil) { (metadata, error) in
            
            //Detecta si ocurrio un error
            if let err = error  {
                print("Ocurrio un error al actualizar la foto de perfil", err)
                return
            }
            
            //Manda a descargar el archivo
            Storage.storage().reference().child("FotoPerfil/\(nombreArvhivo)").downloadURL(completion: { (url, error) in
                
                //Detecta si ocurrio algun error
                if let err = error  {
                    print("Detecta si ocurrio un error", err)
                }
                
                //Caprura la url de la foto de perfil
                guard let fotoPerfilURL = url?.absoluteString else { return }
                
                print("Esta es la URL de la nueva foto de perfil ===>\(fotoPerfilURL)")
                
                //Captura el id del usuario en session
                guard let uid = self.users?.uid else { return }
                
                //Valores a guardar
                let valoresGuardar = ["FotoPerfilURL":fotoPerfilURL,"nombres":first_name,"apellidos":last_name,"username":username]
                
                let valores = [uid:valoresGuardar]
                
                if valores.isEmpty {
                    print("Oops ocurrio un error")
                } else {
                    //Actualiza valores
                    Database.database().reference().child("users").updateChildValues(valores, withCompletionBlock: { (error, ref) in
                        
                        //Detecta si ocurrio algun error
                        if let err = error {
                            print("Ocurrio un error al actualizar los datos", err)
                        } else {
                            print("Perfil actualizado con exito")
                            self.dismiss(animated: true, completion: nil)
                            NotificationCenter.default.post(name: EditProfileController.nameNotify, object: nil)
                        }
                    })
                }
                
            })
        }
    }
    
    //Cancela la edicion del perfil
    @objc fileprivate func CancelEdit() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Oculta la barra de estado
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //Oculata el teclado con tocar en la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
