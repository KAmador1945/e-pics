//
//  UserProfilePicCell.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/7/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class UserProfilePicCell: UICollectionViewCell {
    
    //Referencia y asignacion
    var postData: PostUsr? {
        
        didSet {
            
            //URL de la foto publicada
            guard let fotoURL = postData?.fotoURL else { return }
    
            
            //Descarga la foto por medio de la url
            self.PhotoView.CargaImagen(urlString: fotoURL)
        }
    }
    
    //Image View para mostrar las fotos procedientes del carrete
    let PhotoView: CustumImagenView = {
        let iv = CustumImagenView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    //Inicializa la celda
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Se manda a agregar a la vista
        addSubview(PhotoView)
        
        //Constraint
        NSLayoutConstraint.activate([
            PhotoView.topAnchor.constraint(equalTo: topAnchor),
            PhotoView.leftAnchor.constraint(equalTo: leftAnchor),
            PhotoView.rightAnchor.constraint(equalTo: rightAnchor),
            PhotoView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
