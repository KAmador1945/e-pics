//
//  CameraController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/21/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import AVFoundation

@available(iOS 10.0, *)

class CameraController: UIViewController, AVCapturePhotoCaptureDelegate, UIViewControllerTransitioningDelegate {
    
    //Boton captura foto
    let CapturePhotoBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "circle.png")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(TomaFoto), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let BackBtn: UIButton = {
        let btn = UIButton(type: .system)
        let img = UIImage(named: "back.png")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(BackCtrlBtn), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Accede al protocolo delegate
        transitioningDelegate = self
        
        //Controla las sesiones para la camara
        ControlaSesionCamara()
        UbicaCapturePic()
    }
    
    //Controla la animacion para mostrar la camara
    let customAnimacion = CustomAnimacion()
    let customAnimacionDismiss = CustomAnimacionDismiss()
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        //Retorna el valor de la animacion
        return customAnimacion
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customAnimacionDismiss
    }
    
    //Global
    let output = AVCapturePhotoOutput()
    
    //Controla la sesion de la camara
    fileprivate func ControlaSesionCamara() {
        //captura la entrada y salida de datos del dispositivo
        let captureSesion = AVCaptureSession()
        
        //1 prepara la entrada de datos
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
             return
        }
        
        //Captura potencial error
        do {
            //Captura la entrada de datos de media
            let input = try AVCaptureDeviceInput(device: captureDevice)
            //Checha que se haya agregado input data al dispositivo
            if captureSesion.canAddInput(input) {
                captureSesion.addInput(input)
            }
        } catch let error {
            print("No se puede controlar la camara:", error)
        }
        
        //2 Prepara la salida de datos
        
        //Indica si el dato tiene salida y puede ser agregado a la sesion
        if captureSesion.canAddOutput(output) {
            captureSesion.addOutput(output)
        }
        
        //3 prepara el preview
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSesion)
        previewLayer.frame = view.frame
        //Agrega la vista
        view.layer.addSublayer(previewLayer)
        
        //Se inicializa el metodo para mostrar la camara
        captureSesion.startRunning()
    }
    
    //Manda a ubiacar el boton de capturar foto
    fileprivate func UbicaCapturePic() {
        
        //Se agrega a la vista
        view.addSubview(CapturePhotoBtn)
        view.addSubview(BackBtn)
        
        //Constraint
        NSLayoutConstraint.activate([
            CapturePhotoBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -25),
            CapturePhotoBtn.widthAnchor.constraint(equalToConstant: 60),
            CapturePhotoBtn.heightAnchor.constraint(equalToConstant: 60),
            CapturePhotoBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            BackBtn.topAnchor.constraint(equalTo: view.topAnchor, constant: 40),
            BackBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10),
            BackBtn.widthAnchor.constraint(equalToConstant: 30),
            BackBtn.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    //Sale de la camara
    @objc fileprivate func BackCtrlBtn() {
        //Carga home controller
        self.dismiss(animated: true, completion: nil)
    }
    
    //Realiza foto
    @objc fileprivate func TomaFoto() {
       
        //Espesifica las caracteristicas de la foto a tomar
        let settings = AVCapturePhotoSettings()
        
        //Define el formato disponible para la foto
        guard let previewFormatoType = settings.availablePreviewPhotoPixelFormatTypes.first else { return }
        
        
        settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewFormatoType]
        
        //Realiza captura de la foto
        output.capturePhoto(with: settings, delegate: self)
    }
    
    //Salida de la foto
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        //Define el formato de la foto capturada
        guard let imagenData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer!) else { return }
        
        //Almacena el preview de la foto capturada
        let previewPic = UIImage(data: imagenData)
        
        //Accede y ubica el contenedor de la vista
        let containerView = PreviewPicContainerView()
        
        //Desactiva autolayout
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        //Accede al previewImagenView
        containerView.previewImagenView.image = previewPic
        
        //Agrega a la vista
        view.addSubview(containerView)
        
        //Constraint
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            containerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            containerView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
    //Color de la barra de estados
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
