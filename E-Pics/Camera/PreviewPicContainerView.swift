//
//  PreviewPicContainerView.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/25/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Photos

class PreviewPicContainerView: UIView {
    
    //Preview de la foto
    let previewImagenView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    //Cancel imagen button
    let cancelImgBtn: UIButton =  {
       let btn = UIButton(type: .system)
        let img = UIImage(named: "cancel")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(CancelImg), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //Save imagen button
    let saveImgBtn: UIButton = {
        let btn = UIButton()
        let img = UIImage(named: "save")
        btn.setImage(img?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn.addTarget(self, action: #selector(GuardaImg), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    
    //Primera funcion en iniciar
    override init(frame: CGRect) {
        super.init(frame: frame)

        //Manda a llamar las funciones
        UbicaObjeto()
    }
    
    //Ubica los objetos del preview de la camara
    fileprivate func UbicaObjeto() {
        
        //Agrega a la vista
        addSubview(previewImagenView)
        
        //Constraint
        NSLayoutConstraint.activate([
            previewImagenView.topAnchor.constraint(equalTo: topAnchor),
            previewImagenView.bottomAnchor.constraint(equalTo: bottomAnchor),
            previewImagenView.leftAnchor.constraint(equalTo: leftAnchor),
            previewImagenView.rightAnchor.constraint(equalTo: rightAnchor)
        ])
        
        //Agrega a la vista
        addSubview(cancelImgBtn)
        
        //Constraint
        NSLayoutConstraint.activate([
            cancelImgBtn.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            cancelImgBtn.leftAnchor.constraint(equalTo: leftAnchor, constant: 20)
        ])
    
        //Agrega a la vista
        addSubview(saveImgBtn)
        
        //Constraint
        NSLayoutConstraint.activate([
            saveImgBtn.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30),
            saveImgBtn.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    //Manda a cancelar la foto tomada para tomar otra
    @objc fileprivate func CancelImg() {
        //Remueve la vista
        self.removeFromSuperview()
    }
    
    //Manda a guardar la foto al carrete
    @objc fileprivate func GuardaImg() {
        
        guard let previewImg = previewImagenView.image else { return }
        
        //Accede a la libreria y comparte los objetos
        let libreria = PHPhotoLibrary.shared()
        
        //Ejecuta el bloque sincronizadamente
        libreria.performChanges({
            
            //Realiza solicitud para crear el nuevo objeto
            PHAssetChangeRequest.creationRequestForAsset(from: previewImg)
            
        }, completionHandler: { (success, error) in
            //detecta si ocurre algun error
            if let err = error {
                print("Oops algo ocurrio con guardar la foto de perfil", err)
            }
            
            print("Se logro guardar la foto exitosamente", success)
            
            //Frame que se mostrara para mensaje de exito
            DispatchQueue.main.async {
                //Contiene mensaje de guardar
                let savedLbl = UILabel()
                savedLbl.text = "Saved Successfully"
                savedLbl.font = UIFont.boldSystemFont(ofSize: 18)
                savedLbl.textColor = .white
                savedLbl.textAlignment = .center
                savedLbl.layer.cornerRadius = 5
                savedLbl.clipsToBounds = true
                savedLbl.backgroundColor = UIColor(white: 255, alpha: 0.4)
                savedLbl.frame = CGRect(x: 0, y: 0, width: 200, height: 150)
                savedLbl.center = self.center
                
                //Se agrega a la vista
                self.addSubview(savedLbl)
                
                UIView.animate(withDuration: 0.5, delay: 0.75, options: .curveEaseOut, animations: {
                    savedLbl.alpha = 0.0
                }, completion: { (_) in
                    savedLbl.removeFromSuperview()
                })
            }
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
