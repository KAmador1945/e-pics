//
//  CustomAnimacion.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/26/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

//Clase que personaliza la animacion
class CustomAnimacion: NSObject, UIViewControllerAnimatedTransitioning {
    //Controla el tiempo que tarda en realizar la transicion de animacion
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.9
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        //Animacion personalizada
        let ContainerView = transitionContext.containerView
        
        //Retorna espesificamente la vista
        guard let toView = transitionContext.view(forKey: .to) else { return }
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        
        //Agrega a la vista
        ContainerView.addSubview(toView)
        
        //Contiene la ubicacion del frame
        let frameInicial = CGRect(x: -toView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height)
        
        //Accede a la ubicacion del frame
        toView.frame = frameInicial
        
        //Se inicializa la animacion
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            //Posicion del frame
            toView.frame = CGRect(x: 0, y: 0, width: toView.frame.width, height: toView.frame.height)
            fromView.frame = CGRect(x: fromView.frame.width, y: 0, width: fromView.frame.width, height: fromView.frame.height)
        }) { (_) in
            //Notifica al sistema que la animacion ya se ha realizado
            transitionContext.completeTransition(true)
        }
    }
}
