//
//  CommentsController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 3/2/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class CommentsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    //Referencia del arreglo post
    var post: PostUsr?
    
    //TextView para entrar comentarios
    let comentarioText: UITextView = {
        let tv = UITextView()
        tv.textColor = UIColor.gray
        tv.font = UIFont(name: "Avenir Next", size: 17)
        tv.layer.cornerRadius = 5
        tv.layer.borderColor = UIColor.lightGray.cgColor
        tv.layer.borderWidth = 1
        tv.text = "Enter a comment..."
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    //Boton de realizar comentario
    let commentBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Comments", for: .normal)
        btn.setTitleColor(UIColor.lightGray, for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 17)
        btn.setTitleColor(UIColor.purple, for: .normal)
        btn.isEnabled = false
        btn.addTarget(self, action: #selector(RealizaComments), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    
    //Identificador unico de la celda
    let cellID = "CeldaID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        comentarioText.delegate = self
        comentarioText.becomeFirstResponder()
        
        collectionView.backgroundColor = UIColor.white
        
        //Registra la celda
        collectionView.register(CommentsCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -50, right: 0)
        
        //Define que solo el gesto sea vertical
        collectionView.alwaysBounceVertical = true
        
        //Oculta el teclado
        collectionView.keyboardDismissMode = .onDrag
        
        //Define el titulo
        navigationItem.title = "Comments"
        
        //Manda a descargar los comentarios
        DescargaComments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    

    //Contenedor
    lazy var containerView: UIView = {
        
        //Contenedor de vista
        let containerView = UIView()
        containerView.backgroundColor = UIColor.white
        containerView.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        //Se agrega a la vista
        containerView.addSubview(commentBtn)
        
        //Constraint
        NSLayoutConstraint.activate([
            commentBtn.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            commentBtn.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            commentBtn.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -5),
            commentBtn.widthAnchor.constraint(equalToConstant: 90)
        ])
        
        //Se agrega a la vista
        containerView.addSubview(self.comentarioText)
        
        //Constraint
        NSLayoutConstraint.activate([
            self.comentarioText.topAnchor.constraint(equalTo: containerView.topAnchor),
            self.comentarioText.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            self.comentarioText.rightAnchor.constraint(equalTo: commentBtn.leftAnchor, constant: -2),
            self.comentarioText.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 2)
        ])
        
        //linea separadora
        let lineaSeparator = UIView()
        
        lineaSeparator.backgroundColor = UIColor(red: 124/255, green: 124/255, blue: 124/255, alpha: 1)
        lineaSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        //Se agrega a la vista
        containerView.addSubview(lineaSeparator)
        
        //Constraint
        NSLayoutConstraint.activate([
            lineaSeparator.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            lineaSeparator.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 0),
            lineaSeparator.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: 0),
            lineaSeparator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        return containerView
    }()
    
    //Agrega vista como accesoria de la seccion de escribir comentario
    override var inputAccessoryView: UIView? {
        
        //Manda a crear la vista
        get {
            return containerView
        }
    }
    
    //Define el numero de items o celdas
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comments.count
    }
    
    //Se manda a crear las celdas para los items
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Accede a la celda
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CommentsCell
        
        //Se asigna cada celda de comments a su celda correspondiente
        celda.comment = self.comments[indexPath.item]
        
        return celda
    }
    
    //Define el ancho entre las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let celdaComments = CommentsCell(frame: frame)
        celdaComments.comment = comments[indexPath.item]
        celdaComments.layoutIfNeeded()
        
        let targetSize = CGSize(width: view.frame.width, height: 1000)
        let estimedSize = celdaComments.systemLayoutSizeFitting(targetSize)
        
        let height = max(40 + 8 + 8, estimedSize.height)
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    //Define es espacio entre las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    //Settings Textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        DispatchQueue.main.async {
            //Check content of Textview
            if self.comentarioText.text == "Enter a comment..." {
                self.comentarioText.text = ""
                self.comentarioText.textColor = UIColor.purple
                self.commentBtn.isEnabled = true
            }
        }
    }
    
    //Manda a insertar el comentario a firebase
    @objc fileprivate func RealizaComments() {
        
        //commentBtn.isEnabled = false
        
        guard let comentarios = comentarioText.text, comentarios.count > 0 else {
            self.commentBtn.isEnabled = false
            return
        }
        
        //Captura el id del usuario en sesion
        guard let uid = Auth.auth().currentUser?.uid else { return }

        //Captura el id del post que realiza el comentario
        print("postid", self.post?.id ?? "")

        //Captura el id del post
        let postID = self.post?.id ?? ""

        //Captura los valores a insertar en la base de datos
        let valores = ["text": comentarioText.text ?? "", "date":Date().timeIntervalSince1970, "uid":uid] as [String:Any]

        //Metodo firebase para insertar el comentario en la base de datos
        let ref = Database.database().reference().child("comments").child(postID).childByAutoId()
        
        //Manda a insertar los datos a la base de datos
        ref.updateChildValues(valores) { (error, ref) in
            //Detecta si ocurrio algun error
            if let err = error {
                print("Oops, no se puede insertar el comentario", err)
                return
            }
        }
        
        //Ejecuta el bloque en segundo plano
        DispatchQueue.main.async {
            //refresh data
            self.collectionView.reloadData()
            self.comentarioText.text = "Enter a comment..."
            self.comentarioText.textColor = UIColor.lightGray
            self.commentBtn.isEnabled = false
            self.comentarioText.resignFirstResponder()
        }
    }
    
    //Referencia global de comentario
    var comments = [CommentDic]()
    
    //Manda a descargar todos los comentarios realizados
    fileprivate func DescargaComments() {
        //Captura el id del post
        guard let postID = self.post?.id else { return }
        
        //Realiza referencia
        let ref = Database.database().reference().child("comments").child(postID)
        
        //Metodo para descargar la info
        ref.observe(.childAdded, with: { (snapshot) in
            
            //Crea un diccionario
            guard let diccionario = snapshot.value as? [String:Any] else { return }
            
            guard let uid = diccionario["uid"] as? String else { return }
            
            Database.DescargaUsrConUID(uid: uid, completion: { (user) in
                //accede al diccionario
                var comment = CommentDic(user: user,diccionario: diccionario)
                
                //Descarga el comentario por usuario
                comment.user = user
                
                //Se inserta el resultado en un arreglo
                self.comments.append(comment)
                self.collectionView.reloadData()
            })
            
        }) { (error) in
            print("Oops ha ocurrido un error", error)
        }
    }
    
    //Detecta el primer objeto en responder
    override var canBecomeFirstResponder: Bool {
        return true
    }
}
