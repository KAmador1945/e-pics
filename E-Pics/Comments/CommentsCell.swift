//
//  CommentsCell.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 3/2/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class CommentsCell: UICollectionViewCell {
    
    
    //Accede al diccionario
    var comment: CommentDic? {
        didSet {
            
            print(comment?.text as Any)
            print(comment?.uid as Any)
            
            guard let comment = comment else { return }
            let perfilURL = comment.user.fotoPerfilURL
            
            //Manda a cargar el comentario
            
            let atributo = NSMutableAttributedString(string: comment.user.username, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.purple])
            
            atributo.append(NSAttributedString(string: " " + comment.text, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black]))
            
            TextView.attributedText = atributo
            
            Avatar.CargaImagen(urlString: perfilURL)
        }
    }
    
    let TextView: UITextView = {
        let tv = UITextView()
        tv.textColor = UIColor.purple
        tv.isScrollEnabled = false
        tv.isEditable = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    //Ava usuario
    let Avatar: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        let img = CustumImagenView(image: ava)
        img.layer.cornerRadius = 50 / 2
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.layer.borderColor = UIColor.lightGray.cgColor
        img.layer.borderWidth = 1
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Inicializa las celdas
    override init(frame: CGRect) {
        
        //Inicializa la celda
        super.init(frame: frame)
        
        //Color de fondo
        //backgroundColor = .white
        
        //Manda a llamar las funciones
        PreparaElementos()
    }
    
    //Manda a ubicar los elementos
    fileprivate func PreparaElementos() {
        
        //Manda a agregarlo a la vista
        addSubview(Avatar)
        
        //Constraint
        NSLayoutConstraint.activate([
            Avatar.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            Avatar.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            Avatar.widthAnchor.constraint(equalToConstant: 50),
            Avatar.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        //Agrega a la vista
        addSubview(TextView)
        
        //Constraint
        NSLayoutConstraint.activate([
            TextView.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            TextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 60),
            TextView.rightAnchor.constraint(equalTo: rightAnchor, constant: -3),
            TextView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4),
        ])
        
        //line separator
        let separator = UIView()
        separator.backgroundColor = UIColor(white: 0, alpha: 0.5)
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        //add to view
        addSubview(separator)
        
        //constraint
        NSLayoutConstraint.activate([
            separator.leftAnchor.constraint(equalTo: TextView.leftAnchor),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.rightAnchor.constraint(equalTo: rightAnchor),
            separator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
