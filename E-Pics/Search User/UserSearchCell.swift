//
//  UserSearchCell.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/13/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class UserSearchCell: UICollectionViewCell {
    
    var user: User? {
        didSet {
            
            //Agrega el nombre de usuario
            UsernameLbl.text = user?.username
            
            //Accede a la url del avatar del usuario
            guard let urlAvatar = self.user?.fotoPerfilURL else { return }
            
            //Manda a descargar la foto de perfil
            Avatar.CargaImagen(urlString: urlAvatar)
        }
    }
    
    //Carga Avatar
    lazy var Avatar: CustumImagenView = {
        let ava = UIImage(named: "user_avatar.png")
        let img = CustumImagenView(image: ava)
        img.layer.cornerRadius = 60 / 2
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        img.layer.borderColor = UIColor.lightGray.cgColor
        img.layer.borderWidth = 1
        img.isUserInteractionEnabled =  true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    //Username label
    let UsernameLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "Username"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 20)
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let PostLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "12 posts"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next", size: 15)
        lbl.textColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    
    //Inicializa la celda
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Background
        backgroundColor = .white
        
        //Manda a llamar las funciones
        PreparaAvatar()
    }
    
    //Prepara los objetos
    fileprivate func PreparaAvatar() {
        
        //Se agrega  a la vista
        addSubview(Avatar)
        addSubview(UsernameLbl)
        addSubview(PostLbl)
        
        //Constraint
        NSLayoutConstraint.activate([
            Avatar.topAnchor.constraint(equalTo: topAnchor, constant: 3),
            Avatar.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            Avatar.widthAnchor.constraint(equalToConstant: 60),
            Avatar.heightAnchor.constraint(equalToConstant: 60)
        ])
        
        NSLayoutConstraint.activate([
            UsernameLbl.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            UsernameLbl.leftAnchor.constraint(equalTo: Avatar.rightAnchor, constant: 8)
        ])
        
        NSLayoutConstraint.activate([
            PostLbl.topAnchor.constraint(equalTo: topAnchor, constant: 35),
            PostLbl.leftAnchor.constraint(equalTo: Avatar.rightAnchor, constant: 8)
        ])
        
        //Line separator
        let separator = UIView()
        separator.backgroundColor = UIColor(white: 0, alpha: 0.5)
        separator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.leftAnchor.constraint(equalTo: UsernameLbl.leftAnchor),
            separator.rightAnchor.constraint(equalTo: rightAnchor),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
   
    //Manda a actualizar la foto de perfil
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
