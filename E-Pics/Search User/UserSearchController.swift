//
//  UserSearchController.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/13/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit
import Firebase

class UserSearchController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    //Identificador
    let CellID = "CellID"
    
    //Search bar
    lazy var SearchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search User"
        sb.barTintColor = .gray
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        sb.delegate = self
        sb.translatesAutoresizingMaskIntoConstraints = false
        return sb
    }()
    
    
    //Primera funcion en iniciar
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Color de fondo
        collectionView.backgroundColor = .white
        
        //Se registra la clase de la celda
        collectionView.register(UserSearchCell.self, forCellWithReuseIdentifier: CellID)
        
        //Define el scroll vertical
        collectionView.alwaysBounceVertical = true
        
        //Oculta el teclado
        collectionView.keyboardDismissMode = .onDrag
        
        //Manda a llamar las funciones
        SearchNavBar()
        DescargaUsrs()
    }
    
    //Retorna a la vista el objeto
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SearchBar.isHidden = false
    }
    
    fileprivate func SearchNavBar() {
        
        //Inserta la barra de busqueda en la navegacion
        navigationController?.navigationBar.addSubview(SearchBar)
        
        //NavBar object
        guard let navBar = navigationController?.navigationBar else { return }
        
        //Constraint
        NSLayoutConstraint.activate([
            SearchBar.topAnchor.constraint(equalTo: navBar.topAnchor),
            SearchBar.leftAnchor.constraint(equalTo: navBar.leftAnchor, constant: 10),
            SearchBar.rightAnchor.constraint(equalTo: navBar.rightAnchor, constant: -10),
            SearchBar.bottomAnchor.constraint(equalTo: navBar.bottomAnchor)
        ])
    }
    
    //Referencia global
    var filterUsrs = [User]()
    var users = [User]()
    
    //Manda a descargar a los usuarios en las celdas de busqueda
    fileprivate func DescargaUsrs() {
        
        print("Descargando usuarios")
        
        //referencia
        let ref = Database.database().reference().child("users")
        
        //Metodo de descarga
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            //Crea el diccionario
            guard let diccionario = snapshot.value as? [String:Any] else { return }
            
            //Recorre los datos
            diccionario.forEach({ (data) in
                
                //Datos y claves
                let (key, value) = data
                
                //Evita que el usuario conectado este disponible en las busquedas de usuarios
                if key == Auth.auth().currentUser?.uid {
                    print("Se encontro al mismo usuario")
                    return
                }
                
                //Crea el diccionario
                guard let diccionarioUsr = value as? [String:Any] else { return }
                
                //Accede a las claves del diccionario
                let user = User(uid: key, diccionario: diccionarioUsr)
                self.users.append(user)
            })
            
            //Ordena los usuarios
            self.users.sort(by: { (u1, u2) -> Bool in
                return u1.username.compare(u2.username) == .orderedAscending
            })
            
            //Se igualan los valores del arreglo de usuarios a los valores de busqueda
            self.filterUsrs = self.users
            
            //Manda a recargar el collectionview para refrescar datos
            self.collectionView.reloadData()
            
        }) { (error) in
            print("Ocurrio un error al descargar todos los usuarios", error)
        }
    }
    
    //Define el numero de celdas
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterUsrs.count
    }
    
    //Agrega a cada celda su dato correspondiente
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Manda a reutilizar las celdas
        let celda = collectionView.dequeueReusableCell(withReuseIdentifier: CellID, for: indexPath) as! UserSearchCell
        
        //Accede a cada items del arreglo
        celda.user = filterUsrs[indexPath.item]
        
        //Retorna el valor
        return celda
    }
    
    //Detecta que letra se esta escribiendo
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //accede al arreglo que contiene todos los usuarios
        if searchText.isEmpty {
            
            //Se iguala el arreglo de filtro de usuarios a usuarios
            filterUsrs = users
        } else {
            
            //Filtra los usuarios por busqueda
            filterUsrs = self.users.filter { (user) -> Bool in
                return user.username.lowercased().contains(searchText.lowercased())
            }
        }
        
        //Refresca collectionview
        self.collectionView.reloadData()
    }
    
    //Detecta que items fue seleccionado
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Esconde la barra de busqueda
        SearchBar.isHidden = true
        SearchBar.resignFirstResponder()
        
        //Accede a la posicion del objeto
        let userSelected = filterUsrs[indexPath.item]
        print(userSelected)
        
        //Inicializa el controlador del usuario
        let userProfileController = UserProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        
        //Accede al id del usuario seleccionado
        userProfileController.userId = userSelected.uid
        
        //Carga el controlador
        navigationController?.pushViewController(userProfileController, animated: true)
        
    }
    
    //Define las medidas de las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 65)
    }
    
    //Define la separacion de las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
