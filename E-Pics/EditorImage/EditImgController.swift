//
//  EditImg.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 6/2/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class EditImgController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
}
