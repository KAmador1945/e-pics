//
//  ExtentionColor.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 5/19/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

extension UIColor {
    //Extention for main color gray
    static func mainGray() -> UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.7)
    }
    
    //Extention for main color purple
    static func mainPurple() -> UIColor {
        return  UIColor(red: 128/255, green: 0/255, blue: 128/255, alpha: 0.7)
    }
}
