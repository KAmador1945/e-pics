//
//  PostDic.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import Foundation

struct PostUsr {
    
    //Constante del diccionario
    var id: String?
    let user: User
    let fotoURL: String
    let captionPost: String
    let postDate: Date
    
    //Dtermina si el boton fue presionado
    var hasLiked = false
    var noLiked = false
    
    //Inicializa el diccionario
    init(user: User, diccionarioPost: [String:Any]) {
        
        //Se igualan valores
        self.user = user
        self.fotoURL = diccionarioPost["fotoURL"] as? String ?? ""
        self.captionPost = diccionarioPost["caption"] as? String ?? ""
        
        //Se realiza formato para la fecha
        let secondsFrom1970 = diccionarioPost["date"] as? Double ?? 0
        
        //Se procesa la fecha con formato tipo UNIX
        self.postDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}
