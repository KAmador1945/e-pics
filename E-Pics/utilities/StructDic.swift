//
//  StructDic.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 1/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

struct User {
    
    let uid: String
    let nombre: String
    let apellido: String
    let username: String
    let nombreCompleto: String
    let fotoPerfilURL: String
    
    init(uid: String, diccionario: [String:Any]) {
        self.uid = uid
        self.nombre = diccionario["nombres"] as? String ?? ""
        self.apellido = diccionario["apellidos"] as? String ?? ""
        self.username = diccionario["username"] as? String ?? ""
        self.nombreCompleto = "\(diccionario["nombres"] as? String ?? "")" + " " + "\(diccionario["apellidos"] as? String ?? "")"
        self.fotoPerfilURL = diccionario["FotoPerfilURL"] as? String ?? ""
    }
}
