//
//  CustumImagenView.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/10/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

//Captura las iamgenes para crear cache
var CacheImagen = [String: UIImage]()

class CustumImagenView: UIImageView {
    
    //Referencia de la url usada
    var lastURLUsada: String?
    
    //funcion que manda a cargar las fotos
    func CargaImagen(urlString: String) {
        
        
        lastURLUsada = urlString
        
        //Deja ver el background mientras se descarga la foto
        self.image = nil
        
        //Detecta si las fotos ya han pasado a ser almacenada a la cache
        if let CachedImagen = CacheImagen[urlString] {
            self.image = CachedImagen
            return
        }
        
        //Capta la url de la foto en formato String
        guard let url = URL(string: urlString) else { return }
        
        //Realiza la descarga
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            //Detecta si existe algun error
            if let err = error {
                print("Oops algo ha ocurrido", err)
            }
            
            //Detecta si las imagenes han sido descargadas
            if url.absoluteString != self.lastURLUsada {
                return
            }
            
            //Almacena los datos
            guard let dataImagen = data  else { return }
            
            //Almacena el dato de la imagen
            let fotoImagen = UIImage(data: dataImagen)
            
            //Almacena la imagen en el arreglo
            CacheImagen[url.absoluteString] = fotoImagen
            
            //Proceso en segundo plano
            DispatchQueue.main.async {
                //Carga la foto en cada celda
                self.image = fotoImagen
            }
        }.resume()
    }
}
