//
//  extention.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 2/12/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import Foundation
import Firebase

//Extencion para descagar info del usuario con su uid
extension Database {
    
    static func DescargaUsrConUID(uid: String, completion: @escaping (User) -> ()) {
        
        //Accede a la base datos y a la tabla del usuario
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
        
            //Crea un diccionario con datos del usuario
            guard let userDiccionario = snapshot.value as? [String: Any] else { return }
            
            //Inserta el diccionario en el arreglo
            let userData = User(uid: uid, diccionario: userDiccionario)
            
            //Captura datos del usuario
            completion(userData)
            
        }) { (error) in
            print("Ocurrio un error al descargar la info", error)
        }
    }
}
