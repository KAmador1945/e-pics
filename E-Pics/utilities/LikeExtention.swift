//
//  LikeExtention.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 5/27/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import Foundation

struct LikeDicc {
    
    var user: User
    
    let like: String
    let uid: String
    
    init(user: User, diccionario: [String:Any]) {
        self.user = user
        self.like = diccionario["likes"] as? String ?? ""
        self.uid = diccionario["uid"] as? String ?? ""
    }
}
