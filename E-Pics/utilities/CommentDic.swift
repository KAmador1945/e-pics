//
//  CommentDic.swift
//  E-Pics
//
//  Created by Kevin Amador Rios on 3/6/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import Foundation

struct CommentDic {
    
    var user: User
    
    let text: String
    let uid: String
    
    init(user: User, diccionario: [String:Any]) {
        self.user = user
        self.text = diccionario["text"] as? String ?? ""
        self.uid = diccionario["uid"] as? String ?? ""
    }
}
